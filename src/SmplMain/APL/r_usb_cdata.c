/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cdata.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB sample data declaration
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* System definition */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cData.h"			/* Sample Data Header */
#include "r_usb_cExtern.h"			/* USB-FW global define */

/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _smpl

/******************************************************************************
Private global variables and functions
******************************************************************************/
USBC_UTR_t	usb_gcstd_SmplTrnMsg[USB_TBL_MAX];
uint32_t	usb_gcstd_SmplTrnCnt[USB_TBL_MAX];
uint32_t	usb_gcstd_SmplTrnSize[USB_TBL_MAX];
uint8_t		*usb_gcstd_SmplTrnPtr[USB_TBL_MAX];

/* Sample transfer data area */

/* PIPE1 */
uint8_t		usb_gcstd_SmplTrnData1[USB_SMPL_TRNSIZE1
				+ (4 - (USB_SMPL_TRNSIZE1 % 4))];
/* PIPE2 */
uint8_t		usb_gcstd_SmplTrnData2[USB_SMPL_TRNSIZE2
				+ (4 - (USB_SMPL_TRNSIZE2 % 4))];
/* PIPE3 */
uint8_t		usb_gcstd_SmplTrnData3[USB_SMPL_TRNSIZE3
				+ (4 - (USB_SMPL_TRNSIZE3 % 4))];
/* PIPE4 */
uint8_t		usb_gcstd_SmplTrnData4[USB_SMPL_TRNSIZE4
				+ (4 - (USB_SMPL_TRNSIZE4 % 4))];
/* PIPE5 */
uint8_t		usb_gcstd_SmplTrnData5[USB_SMPL_TRNSIZE5
				+ (4 - (USB_SMPL_TRNSIZE5 % 4))];
/* PIPE6 */
uint8_t		usb_gcstd_SmplTrnData6[USB_SMPL_TRNSIZE6
				+ (4 - (USB_SMPL_TRNSIZE6 % 4))];
/* PIPE7 */
uint8_t		usb_gcstd_SmplTrnData7[USB_SMPL_TRNSIZE7
				+ (4 - (USB_SMPL_TRNSIZE7 % 4))];
/* PIPE8 */
uint8_t		usb_gcstd_SmplTrnData8[USB_SMPL_TRNSIZE8
				+ (4 - (USB_SMPL_TRNSIZE8 % 4))];
/* PIPE9 */
uint8_t		usb_gcstd_SmplTrnData9[USB_SMPL_TRNSIZE9
				+ (4 - (USB_SMPL_TRNSIZE9 % 4))];

/******************************************************************************
Renesas USB Host Driver API functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_GetDescriptorInterface
Description     : Get Report Descriptor
Argument        : uint16_t ReqVal           : Request Value
Return          : none
******************************************************************************/
void usb_cstd_GetDescriptorInterface(USBC_REQUEST_t *parm)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* Set Stall */
	R_usb_pstd_PcdChangeDeviceState( USBC_DO_STALL, USBC_PIPE0
		, (USBC_CB_INFO_t)usb_cstd_DummyFunction );
#endif /* USB_FUNCSEL_PP == USBC_PERI_PP */
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
Function Name   : usb_cstd_ClrMbx
Description     : Clear Message in Mailbox
Argument        : USBC_ID_t mbxid            : Mailbox ID
Return          : none
******************************************************************************/
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_OS_PP
void usb_cstd_ClrMbx(USBC_ID_t mbxid)
{
	USBC_MSG_t	*msg;
	USBC_ER_t	err;

	do
	{
		/* Read mailbox */
		err = USBC_PRCV_MSG(mbxid, &msg);
	}
	while( err == USBC_E_OK );

	if( err != USBC_E_TMOUT )
	{
		/* Read error */
		USBC_PRINTF2("usb_cstd_ClrMbx error: id:%d err:%d\n", mbxid, err);
		while( 1 )
		{
		}
	}
}
#endif	/* USBC_FW_PP == USBC_FW_OS_PP */

/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
