/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************
* File Name    : r_usb_PMSCdriver.c
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cDefUsr.h"		/* System definition */
#include "r_usb_cMSCdefine.h"		/* USB	Mass Storage Class Header */
#include "r_usb_pMSCdefine.h"		/* Peri Mass Storage Class Driver define */
#include "r_usb_cATAPIdefine.h"		/* Peripheral ATAPI Device extern define */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW grobal define */
#include "r_usb_pMSCextern.h"		/* Peri Mass Storage Class Extern */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */


/******************************************************************************
Section    <Section Difinition> , "Project Sections"
******************************************************************************/
#pragma section _pmsc


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
/* Mass Storage Device Class sequence */
uint8_t			usb_gpmsc_Seq;
/* Mass Storage Device Class CBW Tag */
uint32_t		usb_gpmsc_Tag;
/* Mass Storage Device Class Transfer Length(CBW) */
uint32_t		usb_gpmsc_Dtl;
/* Mass Storage Device Class CBW */
USBC_MSC_CBW_t	usb_gpmsc_Cbw;
/* Mass Storage Device Class CSW */
USBC_MSC_CSW_t	usb_gpmsc_Csw;
/* Mass Storage Device Class */
USBC_PMSC_CBM_t	usb_gpmsc_Massage;
/* Pipe Direction from Host to Device */
uint16_t		usb_gpmsc_Outpipe;
/* Pipe Direction from Device to Host */
uint16_t		usb_gpmsc_Inpipe;


/******************************************************************************
Renesas Abstracted Peripheral Mass Storage Class Driver API functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pmsc_Task
Description     : Peripheral Mass Storage Class main task
Arguments       : USBC_VP_INT_t stacd        : start code
Return value    : none
******************************************************************************/
void usb_pmsc_Task(USBC_VP_INT_t stacd)
{
	USBC_UTR_t	*mess;
	USBC_ER_t	err = 0l;
	uint8_t		uc_lun = 0;		/* Logical Unit Number */

	/* Pipe Transfer */
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	while( 1 )
	{
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
		err = USBC_TRCV_MSG(USB_PMSC_MBX,(USBC_MSG_t**)&mess,(uint16_t)3000);
		if( err == USBC_E_OK )
		{
			if( mess->msginfo == USBC_MASS_STORAGE_RESET )
			{
				/* Please add the processing with the system. */
				R_usb_pmsc_SetConfig();
				/* CallBack function (SND_MSG) */
				(*mess->complete)(mess);
				err = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)mess);
				if( err != USBC_E_OK )
				{
					USBC_PRINTF1(
						"### pmscTask RESET rel_blk error (%ld)\n"
						, err);
				}
			}
			else if( mess->msginfo == USBC_GET_MAX_LUN )
			{
				R_usb_pstd_ControlRead((uint32_t)USBC_MSC_LUN_LENGTH
					, &uc_lun);
				/* CallBack function (SND_MSG) */
				(*mess->complete)(mess);
				err = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)mess);
				if( err != USBC_E_OK )
				{
					USBC_PRINTF1(
						"### pmscTask MAX_LUN rel_blk error (%ld)\n"
						, err);
				}
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
			}
			else if( mess->msginfo == USBC_PMSC_REQ_TRANS_END )
			{
				/* Request End Process */
				R_usb_pstd_ControlEnd(USBC_CTRL_END);
				err = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)mess);
				if(err != USBC_OK)
				{
					USBC_PRINTF0(
					"### Peripheral Mass Storage process rel_blk error\n");
				}
			}
			else if( mess->msginfo == USBC_PMSC_USB_EXECUTE )
			{
				/* CSW(NG) transfer */
				usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_NG, usb_gpmsc_Dtl);
				R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe
					, (uint32_t)USBC_MSC_CSW_LENGTH
					, (uint8_t *)&usb_gpmsc_Csw
					, (USBC_CB_t)&R_usb_pmsc_TransResult);
				usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
				err = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)mess);
				if( err != USBC_OK )
				{
					/* Memory block release failure */
					USBC_PRINTF0(
					"### Peripheral Mass Storage process rel_blk error\n");
				}
			}
			else if( mess->msginfo == USBC_PMSC_ERR_CSW_NG )
			{
				/* CSW(NG) transfer */
				usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_NG, usb_gpmsc_Dtl);
				R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe
					, (uint32_t)USBC_MSC_CSW_LENGTH
					, (uint8_t*)&usb_gpmsc_Csw
					, &R_usb_pmsc_TransResult);
				usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
				err = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)mess);
				if( err != USBC_OK )
				{
					/* Memory block release failure */
					USBC_PRINTF0(
					"### Peripheral Mass Storage process rel_blk error\n");
				}
			}
			else if( mess->msginfo == USBC_PMSC_ERR_PHASE_ERR )
			{
				/* CSW transfer */
				usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_PHASE_ERR
					, usb_gpmsc_Dtl);
				R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe
					, (uint32_t)USBC_MSC_CSW_LENGTH
					, (uint8_t*)&usb_gpmsc_Csw, &R_usb_pmsc_TransResult);
				usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
				err = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)mess);
				if( err != USBC_OK )
				{
					/* Memory block release failure */
					USBC_PRINTF0(
					"### Peripheral Mass Storage process rel_blk error\n");
				}
			}
			else if( mess->msginfo == USBC_PMSC_ERR_SET_STALL )
			{
				/* Set Stall */
				R_usb_pstd_SetStall((USBC_CB_INFO_t)&usb_pmsc_ErrPhaseErr
					, (uint16_t)usb_gpmsc_Inpipe);
				err = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)mess);
				if( err != USBC_OK )
				{
					/* Memory block release failure */
					USBC_PRINTF0(
					"### Peripheral Mass Storage process rel_blk error\n");
				}
#endif // USBC_FW_PP == USBC_FW_NONOS_PP

			}
			else
			{
				switch( usb_gpmsc_Seq )
				{
/* Normal  pmsc_seq Conditions */
				case USBC_PMSC_PCBWRCV:
					if( mess->msginfo == USBC_PMSC_USB2PMSC )
					{
						switch( mess->status )
						{
						/* Normal Status Condotion */
						case USBC_DATA_OK:
						case USBC_DATA_SHT:
						case USBC_DATA_OVR:
							/* Valid check */
							usb_gpmsc_Seq
								= usb_pmsc_CheckValid(mess->tranlen);
							/* Meaningful check */
							usb_gpmsc_Seq
								= usb_pmsc_CheckMeaning(usb_gpmsc_Seq);
							if( usb_gpmsc_Seq == USBC_PMSC_PCHECK )
							{
								/*	Set data length	*/
								usb_gpmsc_Dtl
									= (uint32_t)usb_gpmsc_Cbw.dCBWDTL_Hi
									<< 24;
								usb_gpmsc_Dtl
									|= (uint32_t)usb_gpmsc_Cbw.dCBWDTL_MH
									<< 16;
								usb_gpmsc_Dtl
									|= (uint32_t)usb_gpmsc_Cbw.dCBWDTL_ML
									<< 8;
								usb_gpmsc_Dtl
									|= (uint32_t)usb_gpmsc_Cbw.dCBWDTL_Lo;
								/* Analyze CBWCB Command */
								usb_pmsc_SmpAtapiAnalyzeCbwCb(
									usb_gpmsc_Cbw.CBWCB);
								usb_gpmsc_Seq
									= usb_pmsc_CommandCheck(usb_gpmsc_Seq);
							}
							/* pmsc_seq classification */
							switch( usb_gpmsc_Seq )
							{
							case USBC_PMSC_PDATASND:
							case USBC_PMSC_PDATARCV:
							case USBC_PMSC_PCSWSND:
								usb_pmsc_SmpAtapiCommandExecute(
									usb_gpmsc_Cbw.CBWCB, mess->status
									, &usb_pmsc_AtapiTransResult);
								break;
							case USBC_PMSC_PERROR0:
								usb_pmsc_Error0();
								break;
							case USBC_PMSC_PERROR1:
								usb_pmsc_Error1();
								break;
							case USBC_PMSC_PERROR2:
								usb_pmsc_Error2();
								break;
							case USBC_PMSC_PERROR3:
								usb_pmsc_Error3();
								break;
							case USBC_PMSC_PERROR4:
								usb_pmsc_Error4();
								break;
							case USBC_PMSC_PERROR5:
								usb_pmsc_Error5();
								break;
							default:
								break;
							}
							break;
						/* Abnormal Status Conditions */
						case USBC_DATA_TMO:
							break;
						case USBC_DATA_STOP:
							USBC_PRINTF0(
								"### USBC_PMSC_PCBWRCV detach error\n");
							USBC_DLY_TSK(20);
							break;
						case USBC_DATA_ERR:
						case USBC_DATA_NONE:
						default:
							USBC_PRINTF0("### USBC_PMSC_PCBWRCV error\n");
							USBC_PRINTF1(
								"### pmsc Status error:%x\n",mess->status);
							USBC_PRINTF1(
								"### pmsc Sequence :%x\n",usb_gpmsc_Seq);
							USBC_PRINTF1(
								"### pmsc msginfo:%x\n",mess->msginfo);
							break;
						}	
					}
					else
					{
						/* Abnormal Condition */
						USBC_PRINTF1("### pmsc msginfo error in CBWRCV:%x\n"
							,mess->msginfo);
					}
					break;
				case USBC_PMSC_PDATARCV:
					if( mess->msginfo == USBC_PMSC_USB2PMSC )
					{
						switch( mess->status )
						{
						/* Normal Status Condotion */
						case USBC_DATA_OK:
						case USBC_DATA_SHT:
						case USBC_DATA_OVR:
							usb_pmsc_SmpAtapiCommandExecute(
								usb_gpmsc_Cbw.CBWCB, mess->status
								, &usb_pmsc_AtapiTransResult);
							break;
						/* Abnormal Stauts Conditions */
						case USBC_DATA_TMO:
							USBC_PRINTF0(
								"### USBC_PMSC_PDATARCV timeout error\n");
							break;
						case USBC_DATA_STOP:
							USBC_PRINTF0(
								"### USBC_PMSC_PDATARCV detach error\n");
							USBC_DLY_TSK(20);
							break;
						case USBC_DATA_ERR:
						case USBC_DATA_NONE:
						default:
							USBC_PRINTF0("### USBC_PMSC_PDATARCV error\n");
							USBC_PRINTF1("### pmsc Status error:%x\n"
								, mess->status);
							USBC_PRINTF1("### pmsc Sequence :%x\n"
								, usb_gpmsc_Seq);
							USBC_PRINTF1("### pmsc msginfo:%x\n"
								, mess->msginfo);
							break;
						}
					}
					else if( mess->msginfo == USBC_PMSC_PFLASH2PMSC )
					{
						/* Next treatment after ATAPI Command Execute */
						usb_pmsc_UsbExecute(mess);
					}
					else
					{
						/* ???? */
						usb_pmsc_UsbExecute(mess);
					}
					break;
				case USBC_PMSC_PDATASND:
					if( mess->msginfo == USBC_PMSC_USB2PMSC )
					{
						switch( mess->status )
						{
						/* Normal Status Condotion */
						case USBC_DATA_NONE:
							usb_pmsc_SmpAtapiCommandExecute(
								usb_gpmsc_Cbw.CBWCB, mess->status
								, &usb_pmsc_AtapiTransResult);
							break;
						/* Abnormal Status Conditions */
						case USBC_DATA_TMO:
							USBC_PRINTF0(
								"### USBC_PMSC_PDATASND timeout error\n");
							break;
						case USBC_DATA_STOP:
							USBC_PRINTF0(
								"### USBC_PMSC_PDATASND detach error\n");
							USBC_DLY_TSK(20);
							break;
						case USBC_DATA_SHT:
						case USBC_DATA_OVR:
						case USBC_DATA_ERR:
						case USBC_DATA_OK:
						default:
							USBC_PRINTF0("### USBC_PMSC_PDATASND error\n");
							USBC_PRINTF1("### pmsc Status error:%x\n"
								, mess->status);
							USBC_PRINTF1("### pmsc Sequence :%x\n"
								, usb_gpmsc_Seq);
							USBC_PRINTF1("### pmsc msginfo:%x\n"
								, mess->msginfo);
							break;
						}
					}
					else if( mess->msginfo == USBC_PMSC_PFLASH2PMSC )
					{
						/* Next treatment after ATAPI Command Execute */
						usb_pmsc_UsbExecute(mess);
					}
					else
					{
						/* ???? */
						usb_pmsc_UsbExecute(mess);
					}
					break;
				case USBC_PMSC_PCSWSND:
					if( mess->msginfo == USBC_PMSC_USB2PMSC )
					{
						switch( mess->status )
						{
						/* Normal  Status Condotion */
						case USBC_DATA_NONE:
							R_usb_pmsc_SetConfig();
							break;
						/* Abnormal Status Conditions */
						case USBC_DATA_TMO:
							USBC_PRINTF0(
								"### USBC_PMSC_PCSWSND timeout error\n");
							break;
						case USBC_DATA_STOP:
							USBC_PRINTF0(
								"### USBC_PMSC_PCSWSND detach error\n");
							USBC_DLY_TSK(20);
							break;
						case USBC_DATA_SHT:
						case USBC_DATA_OVR:
						case USBC_DATA_ERR:
						case USBC_DATA_OK:
						default:
							USBC_PRINTF0("### USBC_PMSC_PCSWSND error\n");
							USBC_PRINTF1("### pmsc Status error:%x\n"
								, mess->status);
							USBC_PRINTF1("### pmsc Sequence :%x\n"
								, usb_gpmsc_Seq);
							USBC_PRINTF1("### pmsc msginfo:%x\n"
								, mess->msginfo);
							break;
						}
					}
					else
					{
						/* Next treatment after ATAPI Command Execute */
						usb_pmsc_UsbExecute(mess);
					}
					break;
				/* Abnormal pmsc_seq Conditions */
				case USBC_PMSC_PDATASNDWAIT:
				case USBC_PMSC_PDATARCVWAIT:
				case USBC_PMSC_PERROR0:
				case USBC_PMSC_PERROR1:
				case USBC_PMSC_PERROR2:
				case USBC_PMSC_PERROR3:
				case USBC_PMSC_PERROR4:
				case USBC_PMSC_PERROR5:
				case USBC_PMSC_PCHECK:
				default:
					USBC_PRINTF1("### pmsc Sequence error:%x\n"
						, usb_gpmsc_Seq);
					break;
				}
			}
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
		}
		else if( err == USBC_E_TMOUT )	/* Timeout Error */
		{
			switch( usb_gpmsc_Seq )
			{
			case USBC_PMSC_PCBWRCV:
				break;
			case USBC_PMSC_PDATARCV:
			case USBC_PMSC_PDATASND:
			case USBC_PMSC_PCSWSND:
			case USBC_PMSC_PDATASNDWAIT:
			case USBC_PMSC_PDATARCVWAIT:
			case USBC_PMSC_PERROR0:
			case USBC_PMSC_PERROR1:
			case USBC_PMSC_PERROR2:
			case USBC_PMSC_PERROR3:
			case USBC_PMSC_PERROR4:
			case USBC_PMSC_PERROR5:
			case USBC_PMSC_PCHECK:
			default:
				USBC_PRINTF0("### DATARCV timeout errox\n");
			}
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
		}
		else
		{
			USBC_PRINTF0("Peripheral strage driver rcv_msg error !\n");
			return;
		}
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	}
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_CheckValid
Description     : Valid CBW Check
Arguments       : uint32_t length           : length
Return value    : uint8_t                   : result
******************************************************************************/
uint8_t	usb_pmsc_CheckValid(uint32_t length)
{
	uint8_t		result;

	result = USBC_PMSC_PCHECK;
	usb_gpmsc_Tag = usb_gpmsc_Cbw.dCBWTag;

	/*	CBW packet valiable check	 */
	if( usb_gpmsc_Cbw.dCBWSignature != USBC_MSC_CBW_SIGNATURE )
	{
		result = USBC_PMSC_PERROR0;
	}

	/* CBW Packet Length Check */
	if( length != 0ul )
	{
		result = USBC_PMSC_PERROR0;
	}

	return result;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_CheckMeaning
Description     : Check the meaning of received CBW
Arguments       : uint8_t seq               : sequence
Return value    : uint8_t                   : result
******************************************************************************/
uint8_t usb_pmsc_CheckMeaning(uint8_t seq)
{
	uint8_t		result;

	if( seq != USBC_PMSC_PCHECK )
	{
		return seq;
	}
	result = USBC_PMSC_PCHECK;

	/*	Check reserved bit	 */
	if( usb_gpmsc_Cbw.bmCBWFlags.reserved7 != 0 )
	{
		result = USBC_PMSC_PERROR1;
	}
	/*	Check reserved bit	 */
	if( usb_gpmsc_Cbw.bCBWLUN.reserved4 != 0 )
	{
		result = USBC_PMSC_PERROR1;
	}
	/*	Check reserved bit	 */
	if( usb_gpmsc_Cbw.bCBWCBLength.reserved3 != 0 )
	{
		result = USBC_PMSC_PERROR1;
	}

	/*	Check Logical Unit Number	*/
	if( usb_gpmsc_Cbw.bCBWLUN.bCBWLUN > 0 )
	{
		result = USBC_PMSC_PERROR1;
	}

	/*	Check Command Block Length	*/
	if( usb_gpmsc_Cbw.bCBWCBLength.bCBWCBLength > usb_gpmsc_CbwCbLength )
	{
		result = USBC_PMSC_PERROR1;
	}
	return result;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_UsbExecute
Description     : Next treatment after ATAPI Command Execute
Arguments       : USBC_UTR_t *mess           : message
Return value    : none
******************************************************************************/
void usb_pmsc_UsbExecute(USBC_UTR_t *mess)
{
	uint32_t	gul_pmsc_size;
	void		*gvp_pmsc_adr;

	switch( mess->status )
	{
	/* Command Execute -> Pass! */
	case USBC_PMSC_CMD_COMPLETE:
		/* CSW(OK) transfer */
		usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_OK, usb_gpmsc_Dtl);
		R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe, (uint32_t)USBC_MSC_CSW_LENGTH
			, (uint8_t*)&usb_gpmsc_Csw, &R_usb_pmsc_TransResult);
		usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
		break;

	/* Command Execute -> Fail */
	case USBC_PMSC_CMD_FAILED:
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
		if( usb_gpmsc_Dtl != 0ul )
		{	/* Zero or short packet */
			R_usb_pstd_SetStall(&usb_pmsc_UsbExecuteCont
				, usb_gpmsc_Inpipe);
		}
		else
		{
			/* CSW(NG) transfer */
			usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_NG, usb_gpmsc_Dtl);
			R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe
				, (uint32_t)USBC_MSC_CSW_LENGTH, (uint8_t*)&usb_gpmsc_Csw
				, &R_usb_pmsc_TransResult);
			usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
		}
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
		if( usb_gpmsc_Dtl != 0ul )
		{	/* Zero or short packet */
			R_usb_pstd_SetStall(&R_usb_pmsc_StallClearResult
				, usb_gpmsc_Inpipe);
			R_usb_pmsc_ProcessWaitTmo((uint16_t)1000);
		}
		/* CSW(NG) transfer */
		usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_NG, usb_gpmsc_Dtl);
		R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe
			, (uint32_t)USBC_MSC_CSW_LENGTH, (uint8_t*)&usb_gpmsc_Csw
			, &R_usb_pmsc_TransResult);
		usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
		break;

	/* Set Send /Recieve Data parameters */
	case USBC_PMSC_CMD_CONTINUE:
		switch( usb_gpmsc_Seq )
		{
		case USBC_PMSC_PDATARCV:		/* OUT Data */
			gul_pmsc_size	= mess->tranlen;
			gvp_pmsc_adr	= mess->tranadr;
			/* Data receive start */
			R_usb_pmsc_DataTrans(usb_gpmsc_Outpipe, gul_pmsc_size
				, (uint8_t*)gvp_pmsc_adr, &R_usb_pmsc_TransResult);
			usb_gpmsc_Seq = USBC_PMSC_PDATARCV;
			break;
		case USBC_PMSC_PDATASND:		/* IN Data */
			gul_pmsc_size	= mess->tranlen;
			gvp_pmsc_adr	= mess->tranadr;
			/* Data transfer start */
			R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe, gul_pmsc_size
				,(uint8_t*)gvp_pmsc_adr, &R_usb_pmsc_TransResult);
			usb_gpmsc_Seq = USBC_PMSC_PDATASND;
			break;

		case USBC_PMSC_PDATARCVWAIT:
		case USBC_PMSC_PDATASNDWAIT:
		case USBC_PMSC_PCSWSND:
		default:
			USBC_PRINTF0("### usb_pmsc_UsbExecute PMSC error:\n");
			break;
		}
		break;

	case USBC_PMSC_CMD_ERROR:
		USBC_PRINTF2("### usb_pmsc_UsbExecute CMD error1:%d, %x\n"
			, usb_gpmsc_Seq,mess->status);
		USBC_PRINTF1("### usb_gpmsc_Massage:%08x\n", mess->msghead);
		USBC_PRINTF3("### msginfo:%04x, keyword:%04x, status:%04x\n"
			, mess->msginfo,mess->keyword,mess->status);
		USBC_PRINTF2("### *tranadr:%08x, tranlen:%d\n", &mess->tranadr
			, mess->tranlen);
		USBC_PRINTF0("### error\n");
		break;
	default:

		USBC_PRINTF2("### usb_pmsc_UsbExecute CMD error2:%d, %x\n"
			, usb_gpmsc_Seq,mess->status);
		USBC_PRINTF1("### usb_gpmsc_Massage:%08x\n",mess->msghead);
		USBC_PRINTF3("### msginfo:%04x, keyword:%04x, status:%04x\n"
			, mess->msginfo,mess->keyword,mess->status);
		USBC_PRINTF2("### *tranadr:%08x, tranlen:%d\n", &mess->tranadr
			, mess->tranlen);
		USBC_PRINTF0("### error\n");
		break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_CommandCheck
Description     : Check Analyze_CBWCB result
Arguments       : uint8_t seq               : sequence
Return value    : uint8_t                   : result
******************************************************************************/
uint8_t usb_pmsc_CommandCheck(uint8_t seq)
{
	uint8_t		uc_case, result, uc_pmsc_case;

	if( seq != USBC_PMSC_PCHECK )
	{
		return seq;
	}

	result = USBC_PMSC_PCHECK;
	/* Mass Storage Device Class Command case */
	uc_pmsc_case = USBC_MSC_CASE00;

	/* Command check */
	switch( usb_gpmsc_Massage.ar_rst )
	{
		case USBC_ATAPI_NO_DATA:				/* No data commnad */
			uc_case = USBC_MSC_DNXX;
			break;
		case USBC_ATAPI_SND_DATAS:			/* Send data command */
			uc_case = USBC_MSC_DIXX;
			break;
		case USBC_ATAPI_RCV_DATAS:			/* Receive data command */
			uc_case = USBC_MSC_DOXX;
			break;
		case USBC_ATAPI_COMMAND_ANALYZE_ERROR:	/* Command analyze error */

			if( usb_gpmsc_Dtl == 0ul )
			{
				result = USBC_PMSC_PERROR5;	/* No STALL Pipe */
			}
			else
			{
				if( usb_gpmsc_Cbw.bmCBWFlags.CBWdir == 1 )
				{
					result = USBC_PMSC_PERROR1;	/* IN Pipe Stall */
				}
				else
				{
					result = USBC_PMSC_PERROR2;	/* OUT Pipe Stall */
				}
			}
			break;
		case USBC_ATAPI_CMD_CONTINUE:
		case USBC_ATAPI_CMD_COMPLETE:
		case USBC_ATAPI_ERROR:
		default:
			USBC_PRINTF0("### Unexpcted Command Check Error\n");
			result = USBC_PMSC_PERROR1;
			break;
	}
	if( result == USBC_PMSC_PCHECK )
	{
		/* Check 13 case */
		uc_pmsc_case = usb_pmsc_CheckCase13(usb_gpmsc_Massage.ul_size
			, &uc_case);
		/* Decide response according to 13 case */
		result = usb_pmsc_TransferMatrix(uc_pmsc_case);
	}
	return result;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_CheckCase13
Description     : 13 case check
Arguments       : uint32_t ul_size          : size
                : uint8_t *uc_case          : case
Return value    : uint8_t                   : result
******************************************************************************/
uint8_t usb_pmsc_CheckCase13(uint32_t ul_size, uint8_t *uc_case)
{
	uint8_t		result;

	if( usb_gpmsc_Dtl == 0ul )
	{
		*uc_case |= (uint8_t)USBC_MSC_XXHN;	/* Host No Data */
	}
	else if( usb_gpmsc_Cbw.bmCBWFlags.CBWdir != 0 )
	{
		*uc_case |= (uint8_t)USBC_MSC_XXHI;	/* Host Recieved(IN) Data */
	}
	else
	{
		*uc_case |= (uint8_t)USBC_MSC_XXHO;	/* Host Send(OUT) Data */
	}

/* 13cases */
	switch( *uc_case )
	{
	case USBC_MSC_DNHN:	/* Device No Data */
		result = USBC_MSC_CASE01;
		break;
	case USBC_MSC_DNHI:	/* Device No Data & Host Recieved(IN) Data */
		result = USBC_MSC_CASE04;
		break;
	case USBC_MSC_DNHO:	/* Device No Data & Host Send(OUT) Data */
		result = USBC_MSC_CASE09;
		break;
	case USBC_MSC_DIHN:	/* Device Send(IN) Data & Host No Data */
		result= USBC_MSC_CASE02;
		break;
	case USBC_MSC_DIHI:	/* Device Send(IN) Data & Host Recieved(IN) Data */
		if( usb_gpmsc_Dtl > (ul_size) )
		{
			result = USBC_MSC_CASE05;
		}
		else
		{
			if( usb_gpmsc_Dtl == (ul_size) )
			{
				result= USBC_MSC_CASE06;
			}
			else
			{
				result= USBC_MSC_CASE07;
			}
		}
		break;
	case USBC_MSC_DIHO:	/* Device Send(IN) Data & Host Send(OUT) Data */
		result= USBC_MSC_CASE10;
		break;
	case USBC_MSC_DOHN:	/* Device Recieved(OUT) Data & Host No Data */
		result= USBC_MSC_CASE03;
		break;
	case USBC_MSC_DOHI:	/* Device Recieved(OUT) Data 
									& Host Recieved(IN) Data */
		result=	 USBC_MSC_CASE08;
		break;
	case USBC_MSC_DOHO:	/* Device Recieved(OUT) Data 
									& Host Send(OUT) Data */
		if( usb_gpmsc_Dtl > (ul_size ) )
		{
			result = USBC_MSC_CASE11;
		}
		else
		{
			if( usb_gpmsc_Dtl == (ul_size ) )
			{
				result = USBC_MSC_CASE12;
			}
			else
			{
				result = USBC_MSC_CASE13;
			}
		}
		break;
	default:
		USBC_PRINTF0("### (Error) Not Found 13 Case \n");
		result = USBC_MSC_CASE00;
		break;
	}

	return result;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_TransferMatrix
Description     : Decide response according to 13 case
Arguments       : uint8_t uc_case           : case
Return value    : uint8_t                   : result
******************************************************************************/
uint8_t usb_pmsc_TransferMatrix(uint8_t uc_case)
{
	uint8_t		result;

	switch ( uc_case )
	{
	case USBC_MSC_CASE01:			/* CSW transfer */
		result = USBC_PMSC_PCSWSND;	/* Transfer Command Status Wrapper */
		break;
	case USBC_MSC_CASE05:
	case USBC_MSC_CASE06:			/* Data Transfer(IN) */
		result = USBC_PMSC_PDATASND;	/* Data Transport */
		break;
	case USBC_MSC_CASE12:			/* Data Transfer(OUT) */
		result = USBC_PMSC_PDATARCV;	/* Data Transport */
		break;
	case USBC_MSC_CASE04:			/* CSW transfer */
		result = USBC_PMSC_PERROR1;	/* Data Transfer(IN) */
		break;
	case USBC_MSC_CASE09:			/* CSW transfer */
	case USBC_MSC_CASE11:			/* Data Transfer(OUT) */
		result = USBC_PMSC_PERROR2;
		break;
	case USBC_MSC_CASE02:
	case USBC_MSC_CASE03:
	case USBC_MSC_CASE07:
	case USBC_MSC_CASE08:
		result = USBC_PMSC_PERROR3;
		break;
	case USBC_MSC_CASE10:
	case USBC_MSC_CASE13:
		result = USBC_PMSC_PERROR4;
		break;
	case USBC_MSC_CASE00:
	default:
		usb_gpmsc_Dtl = 0x00ul;
		result = USBC_PMSC_PERROR4;	/* Internal Device Error */
		USBC_PRINTF0("### (Error) Not CASE1-13 \n");
		break;
	}
	return result;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_CswSet
Description     : Create CSW
Arguments       : uint8_t ar_resp           : status
                : uint32_t ul_size          : size
Return value    : none
******************************************************************************/
void usb_pmsc_CswSet(uint8_t ar_resp, uint32_t ul_size)
{
	/* Set CSW parameter */
	usb_gpmsc_Csw.dCSWSignature			= USBC_MSC_CSW_SIGNATURE;
	usb_gpmsc_Csw.dCSWTag				= usb_gpmsc_Tag;
	usb_gpmsc_Csw.dCSWDataResidue_Lo	= (uint8_t)ul_size;
	usb_gpmsc_Csw.dCSWDataResidue_ML	= (uint8_t)(ul_size >> 8);
	usb_gpmsc_Csw.dCSWDataResidue_MH	= (uint8_t)(ul_size >> 16);
	usb_gpmsc_Csw.dCSWDataResidue_Hi	= (uint8_t)(ul_size >> 24);
	usb_gpmsc_Csw.bCSWStatus			= ar_resp;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_MassStrageReset
Description     : Process Mass storage reset request
Arguments       : uint16_t value            : value
                : uint16_t index			: index
                : uint16_t length			: length
Return value    : none
******************************************************************************/
void usb_pmsc_MassStrageReset(uint16_t value, uint16_t index
		, uint16_t length)
{
	if( (value == 0) && (length == 0) )
	{
		USBC_PRINTF0("### Mass Storage Reset Request \n");
		usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
		/* Peripheral Mass Storage Reset request */
		R_usb_pmsc_MassStrageReset(&usb_pmsc_ReqTransEnd);
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
		/* Peripheral Mass Storage Reset request */
		R_usb_pmsc_MassStrageReset(&usb_cstd_ClassProcessResult);
		usb_cstd_ClassProcessWaitTmo((uint16_t)1000);
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	}
	else
	{
		/* Set Stall */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);		/* Req Error */
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_GetMaxLun
Description     : Process Get max LUN request
Arguments       : uint16_t value            : value
                : uint16_t index			: index
                : uint16_t length			: length
Return value    : none
******************************************************************************/
void usb_pmsc_GetMaxLun(uint16_t value, uint16_t index, uint16_t length)
{
	if( value == 0x0000 )
	{
		if( length == USBC_MSC_LUN_LENGTH )
		{
			USBC_PRINTF0("*** GetMaxLun Request \n");
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
			/* Peripheral Get Max LUN request */
			R_usb_pmsc_GetMaxLun(&usb_cstd_DummyFunction);
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
			/* Peripheral Get Max LUN request */
			R_usb_pmsc_GetMaxLun(&usb_cstd_ClassProcessResult);
			usb_cstd_ClassProcessWaitTmo((uint16_t)1000);
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
		}
		else
		{
			/* Set Stall */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);	/* Req Error */
		}
	}
	else
	{
		/* Set Stall */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);		/* Req Error */
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Error0
Description     : Error case 0
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_Error0(void)
{
	USBC_PRINTF0("### ERROR0 \n");
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	/* Wait CBW */
	R_usb_pmsc_SetConfig();
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Error1
Description     : Error case 1
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_Error1(void)
{
	USBC_PRINTF0("### ERROR1 \n");
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	/* Send Stall */
	R_usb_pstd_SetStall((USBC_CB_INFO_t)&usb_pmsc_ErrCswNg
		, usb_gpmsc_Inpipe);
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	/* Set Stall */
	R_usb_pstd_SetStall(&R_usb_pmsc_StallClearResult, usb_gpmsc_Inpipe);
	R_usb_pmsc_ProcessWaitTmo((uint16_t)1000);
	/* CSW(NG) transter */
	usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_NG, usb_gpmsc_Dtl);
	R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe, (uint32_t)USBC_MSC_CSW_LENGTH
		, (uint8_t*)&usb_gpmsc_Csw, &R_usb_pmsc_TransResult);
	usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Error2
Description     : Error case 2
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_Error2(void)
{
	USBC_PRINTF0("### ERROR2 \n");
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	/* Set Stall */
	R_usb_pstd_SetStall((USBC_CB_INFO_t)&usb_pmsc_ErrCswNg
		, usb_gpmsc_Outpipe);
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	/* Set Stall */
	R_usb_pstd_SetStall(&R_usb_pmsc_StallClearResult, usb_gpmsc_Outpipe);
	R_usb_pmsc_ProcessWaitTmo((uint16_t)1000);
	/* CSW(NG) transfer */
	usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_NG, usb_gpmsc_Dtl);
	R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe, (uint32_t)USBC_MSC_CSW_LENGTH
		, (uint8_t*)&usb_gpmsc_Csw, &R_usb_pmsc_TransResult);
	usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Error3
Description     : Error case 3
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_Error3(void)
{
	USBC_PRINTF0("### ERROR3 \n");
	usb_gpmsc_Dtl = 0x00ul;
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	R_usb_pstd_SetStall((USBC_CB_INFO_t)&usb_pmsc_ErrPhaseErr
		, usb_gpmsc_Inpipe);
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	R_usb_pstd_SetStall(&R_usb_pmsc_StallClearResult, usb_gpmsc_Inpipe);
	R_usb_pmsc_ProcessWaitTmo((uint16_t)1000);
	usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_PHASE_ERR, usb_gpmsc_Dtl);
	R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe, (uint32_t)USBC_MSC_CSW_LENGTH
		, (uint8_t*)&usb_gpmsc_Csw, &R_usb_pmsc_TransResult);
	usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Error4
Description     : Error case 4
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_Error4(void)
{
	USBC_PRINTF0("### ERROR4 \n");
	usb_gpmsc_Dtl = 0x00ul;
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	R_usb_pstd_SetStall((USBC_CB_INFO_t)&usb_pmsc_ErrSetStall
		, usb_gpmsc_Outpipe);
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	/* Set Stall */
	R_usb_pstd_SetStall(&R_usb_pmsc_StallClearResult, usb_gpmsc_Outpipe);
	R_usb_pmsc_ProcessWaitTmo((uint16_t)1000);
	/* Set Stall */
	R_usb_pstd_SetStall(&R_usb_pmsc_StallClearResult, usb_gpmsc_Inpipe);
	R_usb_pmsc_ProcessWaitTmo((uint16_t)1000);
	/* CSW transfer */
	usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_PHASE_ERR, usb_gpmsc_Dtl);
	R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe, (uint32_t)USBC_MSC_CSW_LENGTH
		, (uint8_t*)&usb_gpmsc_Csw, &R_usb_pmsc_TransResult);
	usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Error5
Description     : Error case 5
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_Error5(void)
{
	USBC_PRINTF0("### ERROR5 \n");
	/* CSW(NG) transfer */
	usb_pmsc_CswSet((uint8_t)USBC_MSC_CSW_NG, usb_gpmsc_Dtl);
	R_usb_pmsc_DataTrans(usb_gpmsc_Inpipe, (uint32_t)USBC_MSC_CSW_LENGTH
		, (uint8_t*)&usb_gpmsc_Csw, &R_usb_pmsc_TransResult);
	usb_gpmsc_Seq = USBC_PMSC_PCSWSND;
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
