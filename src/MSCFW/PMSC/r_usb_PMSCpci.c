/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************
* File Name    : r_usb_PMSCpci.c
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cDefUsr.h"		/* System definition */
#include "r_usb_cMSCdefine.h"		/* USB	Mass Storage Class Header */
#include "r_usb_pMSCdefine.h"		/* Peri Mass Storage Class Driver define */
#include "r_usbc_cKernelId.h"		/* Kernel ID definition */
#include "r_usb_cATAPIdefine.h"		/* Peripheral ATAPI Device extern define */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacSystemcall.h"	/* uITRON system call macro */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW grobal define */
#include "r_usb_pMSCextern.h"		/* Peri Mass Storage Class Extern */


/******************************************************************************
Section    <Section Difinition> , "Project Sections"
******************************************************************************/
#pragma section _pmsc


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
uint8_t					usb_gpmsc_CbwCbLength;
USBC_UTR_t				usb_gpmsc_Mess;		/* PMSC <--> USB massage */

/* usb_gpmsc_CbwCbLength depends on bInterfaceSubClass parameter.*/
uint8_t usb_gpmsc_InterfaceSubClass[7] =
{
	0,		/* No definition */
	0,		/* (01h:??)	 Reduced Block Command (RBC)T10 (Now Searching) */
	0,		/* (02h:??)	 SFF-8020i (Now Searching) */
	0,		/* (03h:??)	 QIC-157 (Now Searching) */
	0,		/* (04h:12)	 UFI */
	12,		/* (05h:12)	 SFF-8070i */
	0,		/* (06h:chg) SCSI transparent command set (Now Searhing) */
/* 07h-FFh Reserved for future use */
};


/******************************************************************************
Renesas Abstracted Peripheral Mass Storage Class Driver API functions
******************************************************************************/

/******************************************************************************
Function Name   : R_usb_pmsc_Open
Description     : Peripheral Mass Storage Class open function
Arguments       : uint16_t data1            : Not use
                : uint16_t data2            : Not use
Return value    : USBC_ER_t                  : USBC_E_OK etc
******************************************************************************/
USBC_ER_t R_usb_pmsc_Open(uint16_t data1, uint16_t data2)
{
	uint8_t		*ptr;
	uint8_t		subclass;
	uint16_t	conf_num;
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	USBC_ER_t	err;
/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	USBC_TSK_t	tskinfo;
	USBC_MBX_t	mbxinfo;
	USBC_MPL_t	mplinfo;

	USBC_PRINTF0("*** Install PMSC driver ***\n");

	/* Mass Storage Device Class sequence */
	usb_gpmsc_Seq = USBC_PMSC_PCBWRCV;

	/* Create peripheral storage driver task */
	tskinfo.tskatr	= (USBC_ATR_t)(TA_HLNG);			/* Attribute */
	tskinfo.task	= (USBC_FP_t)&usb_pmsc_Task;		/* Start address */
	tskinfo.itskpri	= (USBC_PRI_t)(USBC_PMSC_PRI);	/* Priority */
	tskinfo.stksz	= (USBC_SIZ_t)(USBC_TSK_STK);		/* Stack size */
	tskinfo.stk		= (USBC_VP_t)(NULL);				/* Stack address */
	err = USBC_CRE_TSK(USB_PMSC_TSK, &tskinfo);
	if( err != USBC_E_OK )
	{
		/* Create task failure */
		USBC_PRINTF1("CRE_TSK USB_PMSC_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Create peripheral storage driver mail box */
	/* Attribute */
	mbxinfo.mbxatr	= (USBC_ATR_t)((TA_TFIFO) | (TA_MFIFO));
	/* Priority */
	mbxinfo.maxmpri	= (USBC_PRI_t)(USBC_MBX_PRI);
	/* Header address */
	mbxinfo.mprihd	= (USBC_VP_t)(NULL);
	err = USBC_CRE_MBX(USB_PMSC_MBX, &mbxinfo);
	if( err != USBC_E_OK )
	{
		/* Create mailbox failure */
		USBC_PRINTF1("CRE_MBX USB_PMSC_MBX Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Create peripheral storage driver memory pool */
	mplinfo.mpfatr	= (USBC_ATR_t)(TA_TFIFO);		/* Attribute */
	mplinfo.blkcnt	= (USBC_UINT_t)(USBC_BLK_CNT);	/* Block count */
	mplinfo.blksz	= (USBC_UINT_t)(USBC_BLK_SIZ);	/* Block size */
	mplinfo.mpf		= (USBC_VP_t)( NULL );			/* Start address */
	err = USBC_CRE_MPL(USB_PMSC_MPL, &mplinfo);
	if( err != USBC_E_OK )
	{
		/* Create memorypool failure */
		USBC_PRINTF1("CRE_MPL USB_PMSC_MPL Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}

	/* Start peripheral storage driver	Task */
	err = USBC_STA_TSK(USB_PMSC_TSK, 0);
	if( err != USBC_E_OK )
	{
		/* Start task failure */
		USBC_PRINTF1("STA_TSK USB_PMSC_TSK Error (%ld)\n", err);
		while( 1 )
		{
			/* do nothing */
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP
	/* Start host FW test driver task */
	err = USBC_ACT_TSK(USB_PMSC_TSK);
	if( err != USBC_E_OK )
	{
		/* Active task failure */
		USBC_PRINTF1("ACT_TSK USB_PMSC_TSK Error (%ld)\n", err);
		while( 1 )
		{
		}
	}
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_NOTUSE_PP */

#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */

	/* usb_gpmsc_CbwCbLength parameter set */
	conf_num = usb_pstd_GetConfigNum();
	ptr = usb_gpmsc_ConPtr[conf_num - 1];
	/* Interface Descriptor bInterfaceSubClass */
	subclass = (*(uint8_t*)((uint32_t)ptr + 15ul));

	if( subclass > USBC_PMSC_MAX_SUBCLASS_RANGE )
	{	/* Abnormal Condition */
		USBC_PRINTF0(
		"*** Interface Descriptor bInterfaceSubClass is out of range ! ***\n");
		USBC_PRINTF0("*** Mass Storage Class Not Initialized ***\n");
	}
	else
	{	/* Normal Condition */
		usb_gpmsc_CbwCbLength = usb_gpmsc_InterfaceSubClass[subclass];
		R_usb_pmsc_SetConfig();		/* Receive CBW */
	}
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	return USBC_E_OK;
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	return err;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_Close
Description     : Peripheral Mass Storage Class close function
Arguments       : uint16_t data1            : Not use
                : uint16_t data2            : Not use
Return value    : USBC_ER_t                  : USBC_E_OK etc
******************************************************************************/
USBC_ER_t R_usb_pmsc_Close(uint16_t data1, uint16_t data2)
{
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	return USBC_E_OK;
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	USBC_ER_t	err;

	USBC_PRINTF0("*** Release PMSC driver ***\n");

	/* Stop peripheral storage driver task */
	err = USBC_TER_TSK(USB_PMSC_TSK);

/* Condition compilation by the difference of the device's operating system */
#if USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP
	/* Delete peripheral storage driver memory pool */
	err = USBC_DEL_MPL(USB_PMSC_MPL);

	/* Delete peripheral storage driver mail box */
	err = USBC_DEL_MBX(USB_PMSC_MBX);

	/* Delete peripheral storage driver task */
	err = USBC_DEL_TSK(USB_PMSC_TSK);
#endif	/* USBC_OS_CRE_MODE_PP == USBC_OS_CRE_USE_PP */

	return err;
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_MassStrageReset
Description     : Peripheral Mass Storage Reset request
Arguments       : USBC_CB_INFO_t complete    : Callback function
Return value    : none
******************************************************************************/
void R_usb_pmsc_MassStrageReset(USBC_CB_INFO_t complete)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err, err2;
	USBC_UTR_t	*ptr;

	/* Get memorypool blk */
	err = USBC_PGET_BLK(USB_PMSC_MPL, &p_blf);
	if( err == USBC_E_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead	= (USBC_MH_t)NULL;
		ptr->msginfo	= (uint16_t)USBC_MASS_STORAGE_RESET;
		ptr->keyword	= (uint16_t)0u;
		ptr->status		= (uint16_t)0u;
		ptr->complete	= (USBC_CB_t)complete;	// callback function

		/* Send message */
		err = USBC_SND_MSG(USB_PMSC_MBX, (USBC_MSG_t*)p_blf);
		if( err != USBC_E_OK )
		{
			/* Send message failure */
			USBC_PRINTF1("### pmscMassStrageReset snd_msg error (%ld)\n"
				, err);
			err2 = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)p_blf);
			if( err2 != USBC_E_OK )
			{
				/* Release memory block failure */
				USBC_PRINTF1("### pmscMassStrageReset rel_blk error (%ld)\n"
					, err2);
			}
		}
	}
	else
	{
		/* Get memoryblock failure */
		USBC_PRINTF1("### pmscMassStrageReset pget_blk error (%ld)\n"
			, err);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_GetMaxLun
Description     : Peripheral Get Max LUN request
Arguments       : USBC_CB_INFO_t complete    : 
Return value    : none
******************************************************************************/
void R_usb_pmsc_GetMaxLun(USBC_CB_INFO_t complete)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err, err2;
	USBC_UTR_t	*ptr;

	/* Get mem pool blk */
	err = USBC_PGET_BLK(USB_PMSC_MPL, &p_blf);
	if( err == USBC_E_OK )
	{
		ptr = (USBC_UTR_t*)p_blf;
		ptr->msghead	= (USBC_MH_t)NULL;
		ptr->msginfo	= (uint16_t)USBC_GET_MAX_LUN;
		ptr->keyword	= (uint16_t)0u;
		ptr->status		= (uint16_t)0u;
		ptr->complete	= (USBC_CB_t)complete;

		/* Send message */
		err = USBC_SND_MSG(USB_PMSC_MBX, (USBC_MSG_t*)p_blf);
		if( err != USBC_E_OK )
		{
			/* Send message failure */
			USBC_PRINTF1("### pmscGetMaxLun snd_msg error (%ld)\n", err);
			err2 = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)p_blf);
			if( err2 != USBC_E_OK )
			{
				/* Release memoryblock failure */
				USBC_PRINTF1("### pmscGetMaxLun rel_blk error (%ld)\n"
					, err2);
			}
		}
	}
	else
	{
		/* Get momoryblock failure */
		USBC_PRINTF1("### pmscGetMaxLun pget_blk error (%ld)\n", err);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_SetConfig
Description     : Set_Configuration
Arguments       : none
Return value    : none
******************************************************************************/
void R_usb_pmsc_SetConfig(void)
{
	usb_gpmsc_Dtl = 0x00ul;
	R_usb_pmsc_DataTrans(usb_gpmsc_Outpipe, (uint32_t)USBC_MSC_CBWLENGTH
		, (uint8_t*)&usb_gpmsc_Cbw, &R_usb_pmsc_TransResult);
	/* Receive Command Block Wrapper */
	usb_gpmsc_Seq = USBC_PMSC_PCBWRCV;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_SetInterface
Description     : Set_Interface
Arguments       : uint16_t data1            : Not use
                : uint16_t data2            : Not use
Return value    : none
******************************************************************************/
void R_usb_pmsc_SetInterface(uint16_t data1, uint16_t data2)
{
	usb_gpmsc_Dtl = 0x00ul;
	R_usb_pmsc_DataTrans(usb_gpmsc_Outpipe, (uint32_t)USBC_MSC_CBWLENGTH
		, (uint8_t*)&usb_gpmsc_Cbw, &R_usb_pmsc_TransResult);
	/* Receive Command Block Wrapper */
	usb_gpmsc_Seq = USBC_PMSC_PCBWRCV;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_DescriptorChange
Description     : Descriptor change
Arguments       : uint16_t mode             : Speed mode
                : uint16_t data2            : Not use
Return value    : none
******************************************************************************/
void R_usb_pmsc_DescriptorChange(uint16_t mode, uint16_t data2)
{
	if( mode == USBC_HSCONNECT )
	{
		/* Hi-Speed Mode */
		usb_gpmsc_ConfigrationH1[1]		= USBC_DT_CONFIGURATION;
		usb_gpmsc_ConfigrationF1[1]		= USBC_DT_OTHER_SPEED_CONF;
		/* HIGH */
		usb_gpmsc_ConPtr[0]				= usb_gpmsc_ConfigrationH1;
		/* FULL */
		usb_gpmsc_ConPtrOther[0]		= usb_gpmsc_ConfigrationF1;
		usb_gpmsc_EpTbl1[3]				= 512;
		usb_gpmsc_EpTbl1[USBC_EPL + 3u]	= 512;
	}
	else
	{
		/* Full-Speed Mode */
		usb_gpmsc_ConfigrationF1[1]		= USBC_DT_CONFIGURATION;
		usb_gpmsc_ConfigrationH1[1]		= USBC_DT_OTHER_SPEED_CONF;
		/* FULL */
		usb_gpmsc_ConPtr[0]				= usb_gpmsc_ConfigrationF1;
		/* HIGH */
		usb_gpmsc_ConPtrOther[0]		= usb_gpmsc_ConfigrationH1;
		usb_gpmsc_EpTbl1[3]				= 64;
		usb_gpmsc_EpTbl1[USBC_EPL + 3u]	= 64;
	}

	usb_gpmsc_Inpipe	= usb_gpmsc_EpTbl1[0];
	usb_gpmsc_Outpipe	= usb_gpmsc_EpTbl1[USBC_EPL];
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_DataTrans
Description     : Tranfer data from USB
Arguments       : uint16_t pipe             : Pipe No
                : uint32_t size             : Data Size
                : uint8_t *table            : Data address
                : USBC_CB_t complete         : Callback function
Return value    : none
******************************************************************************/
void R_usb_pmsc_DataTrans(uint16_t pipe, uint32_t size, uint8_t *table
		, USBC_CB_t complete)
{
	/* PIPE Transfer set */
	usb_gpmsc_Mess.keyword		= pipe;
	usb_gpmsc_Mess.tranadr		= table;
	usb_gpmsc_Mess.tranlen		= size;
	usb_gpmsc_Mess.setup		= 0;
	usb_gpmsc_Mess.complete		= complete;
	usb_gpmsc_Mess.segment		= USBC_TRAN_END;
	R_usb_pstd_TransferStart(&usb_gpmsc_Mess);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_TransResult
Description     : CallBack Function
Arguments       : USBC_UTR_t *mess           : message
Return value    : none
******************************************************************************/
void R_usb_pmsc_TransResult(USBC_UTR_t *mess)
{
	USBC_ER_t	err;

	/* From USB device to PMSC */
	mess->msginfo = (uint16_t)USBC_PMSC_USB2PMSC;
	if( mess->status == USBC_DATA_STOP )
	{
		USBC_PRINTF0("### Peripheral Mass Storage driver stoped !\n");
	}
	err = USBC_SND_MSG(USB_PMSC_MBX, (USBC_MSG_t*)mess);
	if( err != USBC_E_OK )
	{
		/* Send message failure */
		USBC_PRINTF1("### pmscTransResult snd_msg error (%ld)\n", err);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_StallClearResult
Description     : CallBack Function
Arguments       : uint16_t data             : keyword
                : uint16_t dummy            : not use
Return value    : none
******************************************************************************/
void R_usb_pmsc_StallClearResult(uint16_t data, uint16_t dummy)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err, err2;
	USBC_UTR_t	*ptr;

	/* Get mem pool blk */
	err = USBC_PGET_BLK(USB_PMSC_MPL, &p_blf);
	if( err == USBC_E_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead	= (USBC_MH_t)NULL;
		ptr->msginfo	= (uint16_t)0u;
		ptr->keyword	= data;
		ptr->status		= (uint16_t)USBC_PMSC_PIPE_STALL_CLEAR;
		/* Send message */
		err = USBC_SND_MSG(USB_PMSC_MBX, (USBC_MSG_t*)p_blf);
		if( err != USBC_E_OK )
		{
			/* Send message failure */
			USBC_PRINTF1("### pmscStallClearResult snd_msg error (%ld)\n"
				, err);
			err2 = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)p_blf);
			if( err2 != USBC_E_OK )
			{
				USBC_PRINTF1("### pmscStallClearResult rel_blk error (%ld)\n"
					, err2);
			}
		}
	}
	else
	{
		/* Get memoryblock faiure */
		USBC_PRINTF1("### pmscStallClearResult pget_blk error (%ld)\n"
			, err);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_ProcessWaitTmo
Description     : CallBack Function
Arguments       : uint16_t tmo              : time out
Return value    : uint16_t                  : status
******************************************************************************/
uint16_t R_usb_pmsc_ProcessWaitTmo(uint16_t tmo)
{
	USBC_UTR_t	*mess;
	USBC_ER_t	err, err2;
	uint16_t	status;

	err = USBC_TRCV_MSG(USB_PMSC_MBX,(USBC_MSG_t**)&mess, (USBC_TM_t)tmo);
	if( err == USBC_E_TMOUT )
	{
		/* Timeout */
		/* USBC_PRINTF0("### pmscProcessWaitTmo rcv_msg time out !\n"); */
		/* return (USBC_ERROR); */
	}
	if( err != USBC_E_OK )
	{
		/* Receive message failure without timeout */
		USBC_PRINTF1("### pmscProcessWaitTmo rcv_msg error (%ld)\n", err);
		/* return (USBC_ERROR); */
	}

	status = mess->status;
	err2 = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)mess);
	if( err2 != USBC_E_OK )
	{
		/* Release memory block failure */
		USBC_PRINTF2("### pmscProcessWaitTmo (%x) rel_blk error (%ld)\n"
			, status, err2);
		return (USBC_ERROR);
	}

	if( err != USBC_E_OK )
	{
		return (USBC_ERROR);
	}
	return (status);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_ControlTrans1
Description     : Process class request (control read)
Arguments       : USBC_REQUEST_t *req        : request
Return value    : none
******************************************************************************/
void R_usb_pmsc_ControlTrans1(USBC_REQUEST_t *req)
{
	if( (req->ReqIndex == 0) && (req->ReqTypeRecip == USBC_INTERFACE) )
	{
		if( req->ReqRequest == USBC_GET_MAX_LUN )
		{
			/* Get MaxLun of Mass storage */
			usb_pmsc_GetMaxLun(req->ReqValue, req->ReqIndex
				, req->ReqLength);
		}
		else
		{
			/* Set Stall */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);		/* Req Error */
		}
	}
	else
	{
		/* Set Stall */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);			/* Req Error */
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_ControlTrans4
Description     : Process class request (control read status stage)
Arguments       : USBC_REQUEST_t *req        : request
Return value    : none
******************************************************************************/
void R_usb_pmsc_ControlTrans4(USBC_REQUEST_t *req)
{
	if( req->ReqRequest == USBC_GET_MAX_LUN )
	{
		usb_cstd_SetBuf((uint16_t)USBC_PIPE0);		/* Set BUF */
	}
	else
	{
		/* Set Stall */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);		/* Req Error */
	}
	R_usb_pstd_ControlEnd((uint16_t)USBC_CTRL_END);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_ControlTrans3
Description     : Process class request (control no data write)
Arguments       : USBC_REQUEST_t *req        : request
Return value    : none
******************************************************************************/
void R_usb_pmsc_ControlTrans3(USBC_REQUEST_t *req)
{
	if( (req->ReqIndex == 0) && (req->ReqTypeRecip == USBC_INTERFACE) )
	{
		if( req->ReqRequest == USBC_MASS_STORAGE_RESET )
		{
			/* Mass storage Reset */
			usb_pmsc_MassStrageReset(req->ReqValue, req->ReqIndex
				, req->ReqLength);
		}
		else
		{
			/* Set Stall */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);		/* Req Error */
		}
	}
	else
	{
		/* Set Stall */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);			/* Req Error */
	}
/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP
	if( req->ReqRequest != USBC_MASS_STORAGE_RESET )
	{
		R_usb_pstd_ControlEnd((uint16_t)USBC_CTRL_END);
	}
#else	/* USBC_FW_PP == USBC_FW_NONOS_PP */
	R_usb_pstd_ControlEnd((uint16_t)USBC_CTRL_END);
#endif	/* USBC_FW_PP == USBC_FW_NONOS_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_ControlTrans0
Description     : Process class request (idle or setup stage)
Arguments       : USBC_REQUEST_t *req        : request
Return value    : none
******************************************************************************/
void R_usb_pmsc_ControlTrans0(USBC_REQUEST_t *req)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_ControlTrans2
Description     : Process class request (control write data stage)
Arguments       : USBC_REQUEST_t *req        : request
Return value    : none
******************************************************************************/
void R_usb_pmsc_ControlTrans2(USBC_REQUEST_t *req)
{
	usb_cstd_SetStall((uint16_t)USBC_PIPE0);				/* Req Error */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : R_usb_pmsc_ControlTrans5
Description     : Process class request (control write status stage)
Arguments       : USBC_REQUEST_t *req        : request
Return value    : none
******************************************************************************/
void R_usb_pmsc_ControlTrans5(USBC_REQUEST_t *req)
{
	usb_cstd_SetStall((uint16_t)USBC_PIPE0);				/* Req Error */
	R_usb_pstd_ControlEnd((uint16_t)USBC_CTRL_END);
}
/******************************************************************************
End of function
******************************************************************************/


/* Condition compilation by the difference of the operating system */
#if USBC_FW_PP == USBC_FW_NONOS_PP

/******************************************************************************
Function Name   : usb_pmsc_UsbExecuteCont
Description     : Execute Continue
Arguments       : uint16_t dat1             : not use
                : uint16_t dat2             : not use
Return value    : none
******************************************************************************/
void usb_pmsc_UsbExecuteCont(uint16_t dat1, uint16_t dat2)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err = USBC_ERROR;
	USBC_UTR_t	*ptr;
	
	/* Get mem pool blk */
	if( USBC_PGET_BLK(USB_PMSC_MPL, &p_blf) == USBC_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead = (USBC_MH_t)NULL;
		ptr->msginfo = (uint16_t)USBC_PMSC_USB_EXECUTE;
		ptr->keyword = (uint16_t)0u;
		ptr->status  = (uint16_t)0u;
		ptr->complete = (USBC_CB_t)NULL;
		/* Send message */
		err = USBC_SND_MSG( USB_PMSC_MBX, (USBC_MSG_t*)p_blf );
		if( err != USBC_OK )
		{
			err = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)p_blf);
			USBC_PRINTF0("### Peripheral UsbExecute snd_msg error\n");
		}
	}
	else
	{
		/* Get memory block failure */
		USBC_PRINTF0("### Peripheral UsbExecute pget_blk error\n");
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_ErrCswNg
Description     : Error CSW
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_ErrCswNg(void)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err = USBC_ERROR;
	USBC_UTR_t	*ptr;
	
	/* Get mem pool blk */
	if( USBC_PGET_BLK(USB_PMSC_MPL, &p_blf) == USBC_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead = (USBC_MH_t)NULL;
		ptr->msginfo = (uint16_t)USBC_PMSC_ERR_CSW_NG;
		ptr->keyword = (uint16_t)0u;
		ptr->status  = (uint16_t)0u;
		ptr->complete = (USBC_CB_t)NULL;
		/* Send message */
		err = USBC_SND_MSG( USB_PMSC_MBX, (USBC_MSG_t*)p_blf );
		if( err != USBC_OK )
		{
			/* Send message failure */
			err = USBC_REL_BLK(USB_PMSC_MPL,(USBC_MH_t)p_blf);
			USBC_PRINTF0("### Peripheral Error snd_msg error\n");
		}
	}
	else
	{
		/* Get memory block failure */
		USBC_PRINTF0("### Peripheral Error pget_blk error\n");
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_ErrPhaseErr
Description     : Phase error
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_ErrPhaseErr(void)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err = USBC_ERROR;
	USBC_UTR_t	*ptr;
	
	/* Get mem pool blk */
	if( USBC_PGET_BLK(USB_PMSC_MPL, &p_blf) == USBC_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead = (USBC_MH_t)NULL;
		ptr->msginfo = (uint16_t)USBC_PMSC_ERR_PHASE_ERR;
		ptr->keyword = (uint16_t)0u;
		ptr->status  = (uint16_t)0u;
		ptr->complete = (USBC_CB_t)NULL;
		/* Send message */
		err = USBC_SND_MSG( USB_PMSC_MBX, (USBC_MSG_t*)p_blf );
		if( err != USBC_OK )
		{
			/* Send message failure */
			err = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)p_blf);
			USBC_PRINTF0("### Peripheral Error snd_msg error\n");
		}
	}
	else
	{
		/* Get memory block failure */
		USBC_PRINTF0("### Peripheral Error pget_blk error\n");
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_ErrSetStall
Description     : Set stall
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pmsc_ErrSetStall(void)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err = USBC_ERROR;
	USBC_UTR_t	*ptr;
	
	/* Get mem pool blk */
	if( USBC_PGET_BLK(USB_PMSC_MPL, &p_blf) == USBC_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead = (USBC_MH_t)NULL;
		ptr->msginfo = (uint16_t)USBC_PMSC_ERR_SET_STALL;
		ptr->keyword = (uint16_t)0u;
		ptr->status  = (uint16_t)0u;
		ptr->complete = (USBC_CB_t)NULL;
		/* Send message */
		err = USBC_SND_MSG( USB_PMSC_MBX, (USBC_MSG_t*)p_blf );
		if( err != USBC_OK )
		{
			/* Send message failure */
			err = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)p_blf);
			USBC_PRINTF0("### Peripheral Error2 snd_msg error\n");
		}
	}
	else
	{
		/* Get memory block failure */
		USBC_PRINTF0("### Peripheral Error2 pget_blk error\n");
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_ReqTransEnd
Description     : Request transmit end
Arguments       : uint16_t dat1             : not use
                : uint16_t dat2             : not use
Return value    : none
******************************************************************************/
void usb_pmsc_ReqTransEnd(uint16_t dat1, uint16_t dat2)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err = USBC_ERROR;
	USBC_UTR_t	*ptr;
	
	/* Get mem pool blk */
	if( USBC_PGET_BLK(USB_PMSC_MPL, &p_blf) == USBC_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead = (USBC_MH_t)NULL;
		ptr->msginfo = (uint16_t)USBC_PMSC_REQ_TRANS_END;
		ptr->keyword = (uint16_t)0u;
		ptr->status  = (uint16_t)0u;
		ptr->complete = (USBC_CB_t)NULL;
		/* Send message */
		err = USBC_SND_MSG( USB_PMSC_MBX, (USBC_MSG_t*)p_blf );
		if( err != USBC_OK )
		{
			/* Send message failure */
			err = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)p_blf);
			USBC_PRINTF0(
			"### Peripheral Mass Storage Reset request snd_msg error\n");
		}
	}
	else
	{
		/* Get memory block failure */
		USBC_PRINTF0(
		"### Peripheral Mass Storage Reset request pget_blk error\n");
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pmsc_Cont
Description     : Request send message
Arguments       : USBC_CB_INFO_t complete    : callback
Return value    : none
******************************************************************************/
void usb_pmsc_Cont(USBC_CB_INFO_t complete)
{
	USBC_MH_t	p_blf;
	USBC_ER_t	err = USBC_ERROR;
	USBC_UTR_t	*ptr;

	/* Get mem pool blk */
	if( USBC_PGET_BLK(USB_PMSC_MPL, &p_blf) == USBC_OK )
	{
		ptr = (USBC_UTR_t*)p_blf; 
		ptr->msghead = (USBC_MH_t)NULL;
		ptr->msginfo = (uint16_t)USBC_GET_MAX_LUN;
		ptr->keyword = (uint16_t)0u;
		ptr->status  = (uint16_t)0u;
		ptr->complete = (USBC_CB_t)complete;  
		/* Send message */
		err = USBC_SND_MSG( USB_PMSC_MBX, (USBC_MSG_t*)p_blf );
		if( err != USBC_OK )
		{
			/* Send message failure */
			err = USBC_REL_BLK(USB_PMSC_MPL, (USBC_MH_t)p_blf);
			USBC_PRINTF0(
				"### Peripheral Get Max LUN request snd_msg error\n");
		}
	}
	else
	{
		/* Get memory block failure */
		USBC_PRINTF0("### Peripheral Get Max LUN request pget_blk error\n");
	}
}
/******************************************************************************
End of function
******************************************************************************/

#endif /* USBC_FW_PP == USBC_FW_NONOS_PP */

/******************************************************************************
End  Of File
******************************************************************************/
