/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************
* File Name    : r_usb_PMSCrequest.c
* Version      : 1.00
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cDefUsr.h"		/* System definition */
#include "r_usbc_cTypedef.h"	/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usb_cMSCdefine.h"		/* USB	Mass Storage Class Header */
#include "r_usb_pMSCdefine.h"		/* Peri Mass Storage Class Driver define */
#include "r_usb_cATAPIdefine.h"		/* Peripheral ATAPI Device extern define */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usb_pMSCextern.h"		/* Peri Mass Storage Class Extern */
#include "r_usb_cExtern.h"			/* USB-FW grobal define */


/******************************************************************************
Section    <Section Difinition> , "Project Sections"
******************************************************************************/
#pragma section _pmsc


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Renesas Abstracted Peripheral Mass Storage Class Driver API functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pmsc_UsrCtrlTransFunction
Description     : Control transfer
Arguments       : USBC_REQUEST_t *ptr        : request
                : uint16_t ctsq             : control stage
Return value    : none
******************************************************************************/
void usb_pmsc_UsrCtrlTransFunction(USBC_REQUEST_t *ptr, uint16_t ctsq)
{
	if( ptr->ReqTypeType == USBC_CLASS )
	{
		switch( ctsq )
		{
		/* Idle or setup stage */
		case USBC_CS_IDST:	R_usb_pmsc_ControlTrans0(ptr);	break;
		/* Control read data stage */
		case USBC_CS_RDDS:	R_usb_pmsc_ControlTrans1(ptr);	break;
		/* Control write data stage */
		case USBC_CS_WRDS:	R_usb_pmsc_ControlTrans2(ptr);	break;
		/* Control write nodata status stage */
		case USBC_CS_WRND:	R_usb_pmsc_ControlTrans3(ptr);	break;
		/* Control read status stage */
		case USBC_CS_RDSS:	R_usb_pmsc_ControlTrans4(ptr);	break;
		/* Control write status stage */
		case USBC_CS_WRSS:	R_usb_pmsc_ControlTrans5(ptr);	break;

		/* Control sequence error */
		case USBC_CS_SQER:
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);	break;
		/* Illegal */
		default:
			R_usb_pstd_ControlEnd((uint16_t)USBC_DATA_ERR);	break;
		}
	}
	else
	{
		USBC_PRINTF2("usb_pmsc_UsrCtrlTransFunction Err: %x %x\n"
			, ptr->ReqTypeType, ctsq);
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
