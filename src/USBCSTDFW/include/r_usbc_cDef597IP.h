/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
******************************************************************************
* File Name    : r_usbc_cDef597IP.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB definition for 597IP
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USBC_CDEF597IP_H__
#define __R_USBC_CDEF597IP_H__


/*****************************************************************************
Structure Types
******************************************************************************/

/* USBC_597IP_PP Register definition */
/* Register and Bit name are united with USBC_597IP_PP */
/* , because the uITRON-FW is compatible. */ 

struct	USBC_REGISTER
{
	volatile uint16_t		SYSCFG0;		/* 00h */
	volatile uint16_t		SYSCFG1;		/* 02h */
	volatile uint16_t		SYSSTS0;		/* 04h */
	volatile uint16_t		SYSSTS1;		/* 06h */
	volatile uint16_t		DVSTCTR0;		/* 08h */
	volatile uint16_t		DVSTCTR1;		/* 0Ah */
	volatile uint16_t		TESTMODE;		/* 0Ch */
	volatile uint16_t		PINCFG;			/* 0Eh */
	volatile uint16_t		DMA0CFG;		/* 10h */
	volatile uint16_t		DMA1CFG;		/* 12h */
	union
	{
		volatile uint16_t	CFIFO16;
		volatile uint8_t	CFIFO08;
	} CFIFOREG;								/* 14h */
	volatile uint16_t		REGISTX12;		/* 16h */
	union
	{
		volatile uint16_t	D0FIFO16;
		volatile uint8_t	D0FIFO08;
	} D0FIFOREG;							/* 18h */
	volatile uint16_t		REGISTX16;		/* 1Ah */
	union
	{
		volatile uint16_t	D1FIFO16;
		volatile uint8_t	D1FIFO08;
	} D1FIFOREG;							/* 1Ch */
	volatile uint16_t		REGISTX1E;		/* 1Eh */
	volatile uint16_t		CFIFOSEL;		/* 20h */
	volatile uint16_t		CFIFOCTR;		/* 22h */
	volatile uint16_t		CFIFOSIE;		/* 24h */
	volatile uint16_t		REGISTX26;		/* 26h(SDLN) */
	volatile uint16_t		D0FIFOSEL;		/* 28h */
	volatile uint16_t		D0FIFOCTR;		/* 2Ah */
	volatile uint16_t		D1FIFOSEL;		/* 2Ch */
	volatile uint16_t		D1FIFOCTR;		/* 2Eh */
	volatile uint16_t		INTENB0;		/* 30h */
	volatile uint16_t		INTENB1;		/* 32h */
	volatile uint16_t		INTENB2;		/* 34h */
	volatile uint16_t		BRDYENB;		/* 36h */
	volatile uint16_t		NRDYENB;		/* 38h */
	volatile uint16_t		BEMPENB;		/* 3Ah */
	volatile uint16_t		SOFCFG;			/* 3Ch */
	volatile uint16_t		REGISTX3E;		/* 3Eh */
	volatile uint16_t		INTSTS0;		/* 40h */
	volatile uint16_t		INTSTS1;		/* 42h */
	volatile uint16_t		INTSTS2;		/* 44h */
	volatile uint16_t		BRDYSTS;		/* 46h */
	volatile uint16_t		NRDYSTS;		/* 48h */
	volatile uint16_t		BEMPSTS;		/* 4Ah */
	volatile uint16_t		FRMNUM;			/* 4Ch */
	volatile uint16_t		UFRMNUM;		/* 4Eh */
	volatile uint16_t		USBADDR;		/* 50h */
	volatile uint16_t		REGISTX52;		/* 52h */
	volatile uint16_t		USBREQ;			/* 54h */
	volatile uint16_t		USBVAL;			/* 56h */
	volatile uint16_t		USBINDX;		/* 58h */
	volatile uint16_t		USBLENG;		/* 5Ah */
	volatile uint16_t		DCPCFG;			/* 5Ch */
	volatile uint16_t		DCPMAXP;		/* 5Eh */
	volatile uint16_t		DCPCTR;			/* 60h */
	volatile uint16_t		REGISTX62;		/* 62h */
	volatile uint16_t		PIPESEL;		/* 64h */
	volatile uint16_t		REGISTX66;		/* 66h */
	volatile uint16_t		PIPECFG;		/* 68h */
	volatile uint16_t		PIPEBUF;		/* 6Ah */
	volatile uint16_t		PIPEMAXP;		/* 6Ch */
	volatile uint16_t		PIPEPERI;		/* 6Eh */

	volatile uint16_t		PIPE1CTR;		/* 70h */
	volatile uint16_t		PIPE2CTR;		/* 72h */
	volatile uint16_t		PIPE3CTR;		/* 74h */
	volatile uint16_t		PIPE4CTR;		/* 76h */
	volatile uint16_t		PIPE5CTR;		/* 78h */
	volatile uint16_t		PIPE6CTR;		/* 7Ah */
	volatile uint16_t		PIPE7CTR;		/* 7Ch */
	volatile uint16_t		PIPE8CTR;		/* 7Eh */
	volatile uint16_t		PIPE9CTR;		/* 80h */
	volatile uint16_t		REGISTX82;		/* 82h */
	volatile uint16_t		REGISTX84;		/* 84h */
	volatile uint16_t		REGISTX86;		/* 86h */
	volatile uint16_t		REGISTX88;		/* 88h */
	volatile uint16_t		REGISTX8A;		/* 8Ah */
	volatile uint16_t		REGISTX8C;		/* 8Ch */
	volatile uint16_t		REGISTX8E;		/* 8Eh */
	volatile uint16_t		PIPE1TRE;		/* 90h */
	volatile uint16_t		PIPE1TRN;		/* 92h */
	volatile uint16_t		PIPE2TRE;		/* 94h */
	volatile uint16_t		PIPE2TRN;		/* 96h */
	volatile uint16_t		PIPE3TRE;		/* 98h */
	volatile uint16_t		PIPE3TRN;		/* 9Ah */
	volatile uint16_t		PIPE4TRE;		/* 9Ch */
	volatile uint16_t		PIPE4TRN;		/* 9Eh */
	volatile uint16_t		PIPE5TRE;		/* A0h */
	volatile uint16_t		PIPE5TRN;		/* A2h */
	volatile uint16_t		REGISTXA4;		/* A4h */
	volatile uint16_t		REGISTXA6;		/* A6h */
	volatile uint16_t		REGISTXA8;		/* A8h */
	volatile uint16_t		REGISTXAA;		/* AAh */
	volatile uint16_t		REGISTXAC;		/* ACh */
	volatile uint16_t		REGISTXAE;		/* AEh */
	volatile uint16_t		REGISTXB0;		/* B0h */
	volatile uint16_t		REGISTXB2;		/* B2h */
	volatile uint16_t		REGISTXB4;		/* B4h */
	volatile uint16_t		REGISTXB6;		/* B6h */
	volatile uint16_t		REGISTXB8;		/* B8h */
	volatile uint16_t		REGISTXBA;		/* BAh */
	volatile uint16_t		REGISTXBC;		/* BCh */
	volatile uint16_t		REGISTXBE;		/* BEh */
	volatile uint16_t		REGISTXC0;		/* C0h(ATEST1) */
	volatile uint16_t		REGISTXC2;		/* C2h(ATEST2) */
	volatile uint16_t		REGISTXC4;		/* C4h(SYNCTEST) */
	volatile uint16_t		REGISTXC6;		/* C6h */
	volatile uint16_t		REGISTXC8;		/* C8h */
	volatile uint16_t		REGISTXCA;		/* CAh */
	volatile uint16_t		REGISTXCC;		/* CCh */
	volatile uint16_t		REGISTXCE;		/* CEh */
	volatile uint16_t		DEVADD0;		/* D0h */
	volatile uint16_t		DEVADD1;		/* D2h */
	volatile uint16_t		DEVADD2;		/* D4h */
	volatile uint16_t		DEVADD3;		/* D6h */
	volatile uint16_t		DEVADD4;		/* D8h */
	volatile uint16_t		DEVADD5;		/* DAh */
	volatile uint16_t		DEVADD6;		/* DCh */
	volatile uint16_t		DEVADD7;		/* DEh */
	volatile uint16_t		DEVADD8;		/* E0h */
	volatile uint16_t		DEVADD9;		/* E2h */
	volatile uint16_t		DEVADDA;		/* E4h */
	volatile uint16_t		REGISTXE6;		/* E6h */
	volatile uint16_t		REGISTXE8;		/* E8h */
	volatile uint16_t		REGISTXEA;		/* EAh */
	volatile uint16_t		REGISTXEC;		/* ECh */
	volatile uint16_t		INVALID_REG;	/* EEh */
};

#endif	/* __R_USBC_CDEF597IP_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
