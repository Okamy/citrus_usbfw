/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
******************************************************************************
* File Name    : r_usbc_cDefRx62nUSBdef.h
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB definition for Rx62n
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/
#ifndef __R_USBC_CUSBDEFBITDEFINE_H__
#define __R_USBC_CUSBDEFBITDEFINE_H__


/*****************************************************************************
Structure Types
******************************************************************************/

/* USBC_597IP_PP Register definition */

/* System Configuration Control Register */
/*#define	USBC_SYSCFG0		(*((REGP*)(USBC_BASE+0x00))) */
/*#define	USBC_SYSCFG1		(*((REGP*)(USBC_BASE+0x02))) */
#define	USBC_XTAL				(0xC000u)	/* b15-14: Crystal selection */
#define	  USBC_XTAL48			 0x8000u		/* 48MHz */
#define	  USBC_XTAL24			 0x4000u		/* 24MHz */
#define	  USBC_XTAL12			 0x0000u		/* 12MHz */
#define	USBC_XCKE				(0x2000u)	/* b13: External clock enable */
#define	USBC_CNTFLG				(0x1000u)	/* b12: Auto clock monitor */
#define	USBC_PLLC				(0x0800u)	/* b11: PLL control */
#define	USBC_SCKE				(0x0400u)	/* b10: USB clock enable */
#define	USBC_PCSDIS				(0x0200u)	/* b9: not CS wakeup */
#define	USBC_LPSME				(0x0100u)	/* b8: Low power sleep mode */
#define	USBC_HSE				(0x0080u)	/* b7: Hi-speed enable */
#define	USBC_DCFM				(0x0040u)	/* b6: Function select */
#define	USBC_DRPD				(0x0020u)	/* b5: D+/D- pull down control */
#define	USBC_DPRPU				(0x0010u)	/* b4: D+ pull up control */
#define	USBC_USBE				(0x0001u)	/* b0: USB module enable */

/* System Configuration Status Register */
/*#define	USBC_SYSSTS0		(*((REGP*)(USBC_BASE+0x04))) */
/*#define	USBC_SYSSTS1		(*((REGP*)(USBC_BASE+0x06))) */
#define	USBC_OVCBIT				(0x8000u)	/* b15-14: Over-current bit */
#define	USBC_OVCMON				(0xC000u)	/* b15-14: Over-current monitor */
#define	USBC_SOFEA				(0x0020u)	/* b5: SOF monitor */
#define	USBC_IDMON				(0x0004u)	/* b2: ID-pin monitor */
#define	USBC_LNST				(0x0003u)	/* b1-0: D+, D- line status */
#define	  USBC_SE1				 0x0003u		/* SE1 */
#define	  USBC_FS_KSTS			 0x0002u		/* Full-Speed K State */
#define	  USBC_FS_JSTS			 0x0001u		/* Full-Speed J State */
#define	  USBC_LS_JSTS			 0x0002u		/* Low-Speed J State */
#define	  USBC_LS_KSTS			 0x0001u		/* Low-Speed K State */
#define	  USBC_SE0				 0x0000u		/* SE0 */

/* Device State Control Register */
/*#define	USBC_DVSTCTR0		(*((REGP*)(USBC_BASE+0x08))) */
/*#define	USBC_DVSTCTR1		(*((REGP*)(USBC_BASE+0x0A))) */
#define	USBC_HNPBTOA			(0x0800u)	/* b11: Host negotiation protocol
											 (BtoA) */
#define	USBC_EXTLP0				(0x0400u)	/* b10: External port */
#define	USBC_VBOUT				(0x0200u)	/* b9: VBUS output */
#define	USBC_WKUP				(0x0100u)	/* b8: Remote wakeup */
#define	USBC_RWUPE				(0x0080u)	/* b7: Remote wakeup sense */
#define	USBC_USBRST				(0x0040u)	/* b6: USB reset enable */
#define	USBC_RESUME				(0x0020u)	/* b5: Resume enable */
#define	USBC_UACT				(0x0010u)	/* b4: USB bus enable */
#define	USBC_RHST				(0x0007u)	/* b2-0: Reset handshake status */
#define	  USBC_HSPROC			 0x0004u		/* HS handshake processing */
#define	  USBC_HSMODE			 0x0003u		/* Hi-Speed mode */
#define	  USBC_FSMODE			 0x0002u		/* Full-Speed mode */
#define	  USBC_LSMODE			 0x0001u		/* Low-Speed mode */
#define	  USBC_UNDECID			 0x0000u		/* Undecided */

/* Test Mode Register */
/*#define	USBC_TESTMODE		(*((REGP*)(USBC_BASE+0x0C))) */
#define	USBC_UTST				(0x000Fu)	/* b3-0: Test select */
#define	  USBC_H_TST_PACKET		 0x000Cu		/* HOST TEST Packet */
#define	  USBC_H_TST_SE0_NAK	 0x000Bu		/* HOST TEST SE0 NAK */
#define	  USBC_H_TST_K			 0x000Au		/* HOST TEST K */
#define	  USBC_H_TST_J			 0x0009u		/* HOST TEST J */
#define	  USBC_H_TST_NORMAL		 0x0000u		/* HOST Normal Mode */
#define	  USBC_P_TST_PACKET		 0x0004u		/* PERI TEST Packet */
#define	  USBC_P_TST_SE0_NAK	 0x0003u		/* PERI TEST SE0 NAK */
#define	  USBC_P_TST_K			 0x0002u		/* PERI TEST K */
#define	  USBC_P_TST_J			 0x0001u		/* PERI TEST J */
#define	  USBC_P_TST_NORMAL		 0x0000u		/* PERI Normal Mode */

/* Data Pin Configuration Register */
/*#define	USBC_PINCFG			(*((REGP*)(USBC_BASE+0x0E))) */
#define	USBC_LDRV				(0x8000u)	/* b15: Drive Current Adjust */
#define	  USBC_VIF1				 0x0000u		/* VIF = 1.8V */
#define	  USBC_VIF3				 0x8000u		/* VIF = 3.3V */
#define	USBC_INTA				(0x0001u)	/* b1: USB INT-pin active */


/* DMAx Pin Configuration Register */
/*#define	USBC_DMA0CFG		(*((REGP*)(USBC_BASE+0x10))) */
/*#define	USBC_DMA1CFG		(*((REGP*)(USBC_BASE+0x12))) */
#define	USBC_DREQA				(0x4000u)	/* b14: Dreq active select */
#define	USBC_BURST				(0x2000u)	/* b13: Burst mode */
#define	USBC_DACKA				(0x0400u)	/* b10: Dack active select */
#define	USBC_DFORM				(0x0380u)	/* b9-7: DMA mode select */
#define	  USBC_CPU_ADR_RD_WR	 0x0000u		/* Address + RD/WR mode
												 (CPU bus) */
#define	  USBC_CPU_DACK_RD_WR	 0x0100u		/* DACK + RD/WR (CPU bus) */
#define	  USBC_CPU_DACK_ONLY	 0x0180u		/* DACK only (CPU bus) */
#define	  USBC_SPLIT_DACK_ONLY	 0x0200u		/* DACK only (SPLIT bus) */
#define	USBC_DENDA				(0x0040u)	/* b6: Dend active select */
#define	USBC_PKTM				(0x0020u)	/* b5: Packet mode */
#define	USBC_DENDE				(0x0010u)	/* b4: Dend enable */
#define	USBC_OBUS				(0x0004u)	/* b2: OUTbus mode */


/* CFIFO/DxFIFO Port Register */
/*#define	USBC_CFIFO			(*((REGP*)(USBC_BASE+0x14))) */
/*#define	USBC_CFIFO_8		(*((REGP8*)(USBC_BASE+0x14))) */
/*#define	USBC_D0FIFO			(*((REGP*)(USBC_BASE+0x18))) */
/*#define	USBC_D0FIFO_8		(*((REGP8*)(USBC_BASE+0x18))) */
/*#define	USBC_D1FIFO			(*((REGP*)(USBC_BASE+0x1C))) */
/*#define	USBC_D1FIFO_8		(*((REGP8*)(USBC_BASE+0x1C))) */

/* CFIFO/DxFIFO Port Select Register */
/*#define	USBC_CFIFOSEL		(*((REGP*)(USBC_BASE+0x20))) */
/*#define	USBC_D0FIFOSEL		(*((REGP*)(USBC_BASE+0x28))) */
/*#define	USBC_D1FIFOSEL		(*((REGP*)(USBC_BASE+0x2C))) */
#define	USBC_RCNT				(0x8000u)	/* b15: Read count mode */
#define	USBC_REW				(0x4000u)	/* b14: Buffer rewind */
#define	USBC_DCLRM				(0x2000u)	/* b13: DMA buffer clear mode */
#define	USBC_DREQE				(0x1000u)	/* b12: DREQ output enable */
#define	USBC_MBW				(0x0400u)	/* b10: Maximum bit width for
													 FIFO access */
#define	  USBC_MBW_8			 0x0000u		/*  8bit */
#define	  USBC_MBW_16			 0x0400u		/* 16bit */
#define	USBC_BIGEND				(0x0100u)	/* b8: Big endian mode */
#define	  USBC_FIFO_LITTLE		 0x0000u		/* Little endian */
#define	  USBC_FIFO_BIG			 0x0100u		/* Big endian */
#define	USBC_ISEL				(0x0020u)	/* b5: DCP FIFO port direction
											 select */
#define	USBC_CURPIPE			(0x000Fu)	/* b2-0: PIPE select */

/* CFIFO/DxFIFO Port Control Register */
/*#define	USBC_CFIFOCTR		(*((REGP*)(USBC_BASE+0x22))) */
/*#define	USBC_D0FIFOCTR		(*((REGP*)(USBC_BASE+0x2A))) */
/*#define	USBC_D1FIFOCTR		(*((REGP*)(USBC_BASE+0x2E))) */
#define	USBC_BVAL				(0x8000u)	/* b15: Buffer valid flag */
#define	USBC_BCLR				(0x4000u)	/* b14: Buffer clear */
#define	USBC_FRDY				(0x2000u)	/* b13: FIFO ready */
#define	USBC_DTLN				(0x0FFFu)	/* b11-0: FIFO data length */


/* Interrupt Enable Register 0 */
/*#define	USBC_INTENB0		(*((REGP*)(USBC_BASE+0x30))) */
#define	USBC_VBSE				(0x8000u)	/* b15: VBUS interrupt */
#define	USBC_RSME				(0x4000u)	/* b14: Resume interrupt */
#define	USBC_SOFE				(0x2000u)	/* b13: Frame update interrupt */
#define	USBC_DVSE				(0x1000u)	/* b12: Device state transition
											 interrupt */
#define	USBC_CTRE				(0x0800u)	/* b11: Control transfer stage
											 transition interrupt */
#define	USBC_BEMPE				(0x0400u)	/* b10: Buffer empty interrupt */
#define	USBC_NRDYE				(0x0200u)	/* b9: Buffer notready interrupt */
#define	USBC_BRDYE				(0x0100u)	/* b8: Buffer ready interrupt */

/* Interrupt Enable Register 1 */
/*#define	USBC_INTENB1		(*((REGP*)(USBC_BASE+0x32))) */
/*#define	USBC_INTENB2		(*((REGP*)(USBC_BASE+0x34))) */
#define	USBC_OVRCRE				(0x8000u)	/* b15: Over-current interrupt */
#define	USBC_BCHGE				(0x4000u)	/* b14: USB bus change interrupt */
#define	USBC_DTCHE				(0x1000u)	/* b12: Detach sense interrupt */
#define	USBC_ATTCHE				(0x0800u)	/* b11: Attach sense interrupt */
#define	USBC_EOFERRE			(0x0040u)	/* b6: EOF error interrupt */
#define	USBC_SIGNE				(0x0020u)	/* b5: SETUP IGNORE interrupt */
#define	USBC_SACKE				(0x0010u)	/* b4: SETUP ACK interrupt */

/* BRDY Interrupt Enable/Status Register */
/*#define	USBC_BRDYENB		(*((REGP*)(USBC_BASE+0x36))) */
/*#define	USBC_BRDYSTS		(*((REGP*)(USBC_BASE+0x46))) */
#define	USBC_BRDY9				(0x0200u)	/* b9: PIPE9 */
#define	USBC_BRDY8				(0x0100u)	/* b8: PIPE8 */
#define	USBC_BRDY7				(0x0080u)	/* b7: PIPE7 */
#define	USBC_BRDY6				(0x0040u)	/* b6: PIPE6 */
#define	USBC_BRDY5				(0x0020u)	/* b5: PIPE5 */
#define	USBC_BRDY4				(0x0010u)	/* b4: PIPE4 */
#define	USBC_BRDY3				(0x0008u)	/* b3: PIPE3 */
#define	USBC_BRDY2				(0x0004u)	/* b2: PIPE2 */
#define	USBC_BRDY1				(0x0002u)	/* b1: PIPE1 */
#define	USBC_BRDY0				(0x0001u)	/* b1: PIPE0 */

/* NRDY Interrupt Enable/Status Register */
/*#define	USBC_NRDYENB		(*((REGP*)(USBC_BASE+0x38))) */
/*#define	USBC_NRDYSTS		(*((REGP*)(USBC_BASE+0x48))) */
#define	USBC_NRDY9				(0x0200u)	/* b9: PIPE9 */
#define	USBC_NRDY8				(0x0100u)	/* b8: PIPE8 */
#define	USBC_NRDY7				(0x0080u)	/* b7: PIPE7 */
#define	USBC_NRDY6				(0x0040u)	/* b6: PIPE6 */
#define	USBC_NRDY5				(0x0020u)	/* b5: PIPE5 */
#define	USBC_NRDY4				(0x0010u)	/* b4: PIPE4 */
#define	USBC_NRDY3				(0x0008u)	/* b3: PIPE3 */
#define	USBC_NRDY2				(0x0004u)	/* b2: PIPE2 */
#define	USBC_NRDY1				(0x0002u)	/* b1: PIPE1 */
#define	USBC_NRDY0				(0x0001u)	/* b1: PIPE0 */

/* BEMP Interrupt Enable/Status Register */
/*#define	USBC_BEMPENB		(*((REGP*)(USBC_BASE+0x3A))) */
/*#define	USBC_BEMPSTS		(*((REGP*)(USBC_BASE+0x4A))) */
#define	USBC_BEMP9				(0x0200u)	/* b9: PIPE9 */
#define	USBC_BEMP8				(0x0100u)	/* b8: PIPE8 */
#define	USBC_BEMP7				(0x0080u)	/* b7: PIPE7 */
#define	USBC_BEMP6				(0x0040u)	/* b6: PIPE6 */
#define	USBC_BEMP5				(0x0020u)	/* b5: PIPE5 */
#define	USBC_BEMP4				(0x0010u)	/* b4: PIPE4 */
#define	USBC_BEMP3				(0x0008u)	/* b3: PIPE3 */
#define	USBC_BEMP2				(0x0004u)	/* b2: PIPE2 */
#define	USBC_BEMP1				(0x0002u)	/* b1: PIPE1 */
#define	USBC_BEMP0				(0x0001u)	/* b0: PIPE0 */

/* SOF Pin Configuration Register */
/*#define	USBC_SOFCFG			(*((REGP*)(USBC_BASE+0x3C))) */
#define	USBC_TRNENSEL			(0x0100u)	/* b8: Select transaction enable
											 period */
#define	USBC_BRDYM				(0x0040u)	/* b6: BRDY clear timing */
#define	USBC_INTL				(0x0020u)	/* b5: Interrupt sense select */
#define	USBC_EDGESTS			(0x0010u)	/* b4:  */
#define	USBC_SOFMODE			(0x000Cu)	/* b3-2: SOF pin select */
#define	  USBC_SOF_125US		 0x0008u		/* SOF 125us Frame Signal */
#define	  USBC_SOF_1MS			 0x0004u		/* SOF 1ms Frame Signal */
#define	  USBC_SOF_DISABLE		 0x0000u		/* SOF Disable */


/* Interrupt Status Register 0 */
/*#define	USBC_INTSTS0		(*((REGP*)(USBC_BASE+0x40))) */
#define	USBC_VBINT				(0x8000u)	/* b15: VBUS interrupt */
#define	USBC_RESM				(0x4000u)	/* b14: Resume interrupt */
#define	USBC_SOFR				(0x2000u)	/* b13: SOF update interrupt */
#define	USBC_DVST				(0x1000u)	/* b12: Device state transition
											 interrupt */
#define	USBC_CTRT				(0x0800u)	/* b11: Control transfer stage
											 transition interrupt */
#define	USBC_BEMP				(0x0400u)	/* b10: Buffer empty interrupt */
#define	USBC_NRDY				(0x0200u)	/* b9: Buffer notready interrupt */
#define	USBC_BRDY				(0x0100u)	/* b8: Buffer ready interrupt */
#define	USBC_VBSTS				(0x0080u)	/* b7: VBUS input port */
#define	USBC_DVSQ				(0x0070u)	/* b6-4: Device state */
#define	  USBC_DS_SPD_CNFG		 0x0070u		/* Suspend Configured */
#define	  USBC_DS_SPD_ADDR		 0x0060u		/* Suspend Address */
#define	  USBC_DS_SPD_DFLT		 0x0050u		/* Suspend Default */
#define	  USBC_DS_SPD_POWR		 0x0040u		/* Suspend Powered */
#define	  USBC_DS_SUSP			 0x0040u		/* Suspend */
#define	  USBC_DS_CNFG			 0x0030u		/* Configured */
#define	  USBC_DS_ADDS			 0x0020u		/* Address */
#define	  USBC_DS_DFLT			 0x0010u		/* Default */
#define	  USBC_DS_POWR			 0x0000u		/* Powered */
#define	USBC_DVSQS				(0x0030u)	/* b5-4: Device state */
#define	USBC_VALID				(0x0008u)	/* b3: Setup packet detect flag */
#define	USBC_CTSQ				(0x0007u)	/* b2-0: Control transfer stage */
#define	  USBC_CS_SQER			 0x0006u		/* Sequence error */
#define	  USBC_CS_WRND			 0x0005u		/* Ctrl write nodata status
												 stage */
#define	  USBC_CS_WRSS			 0x0004u		/* Ctrl write status stage */
#define	  USBC_CS_WRDS			 0x0003u		/* Ctrl write data stage */
#define	  USBC_CS_RDSS			 0x0002u		/* Ctrl read status stage */
#define	  USBC_CS_RDDS			 0x0001u		/* Ctrl read data stage */
#define	  USBC_CS_IDST			 0x0000u		/* Idle or setup stage */

/* Interrupt Status Register 1 */
/*#define	USBC_INTSTS1		(*((REGP*)(USBC_BASE+0x42))) */
/*#define	USBC_INTSTS2		(*((REGP*)(USBC_BASE+0x44))) */
#define	USBC_OVRCR				(0x8000u)	/* b15: Over-current interrupt */
#define	USBC_BCHG				(0x4000u)	/* b14: USB bus change interrupt */
#define	USBC_DTCH				(0x1000u)	/* b12: Detach sense interrupt */
#define	USBC_ATTCH				(0x0800u)	/* b11: Attach sense interrupt */
#define	USBC_EOFERR				(0x0040u)	/* b6: EOF-error interrupt */
#define	USBC_SIGN				(0x0020u)	/* b5: Setup ignore interrupt */
#define	USBC_SACK				(0x0010u)	/* b4: Setup ack interrupt */

/* Frame Number Register */
/*#define	USBC_FRMNUM			(*((REGP*)(USBC_BASE+0x4C))) */
#define	USBC_OVRN				(0x8000u)	/* b15: Overrun error */
#define	USBC_CRCE				(0x4000u)	/* b14: Received data error */
#define	USBC_FRNM				(0x07FFu)	/* b10-0: Frame number */

/* Micro Frame Number Register */
/*#define	USBC_UFRMNUM		(*((REGP*)(USBC_BASE+0x4E))) */
#define	USBC_UFRNM				(0x0007u)	/* b2-0: Micro frame number */


/* USB Address / Low Power Status Recovery Register */
/*#define	USBC_USBADDR		(*((REGP*)(USBC_BASE+0x50))) */
#define	USBC_USBADDR_MASK		(0x007Fu)	/* b6-0: USB address */
#define	USBC_STSRECOV			(0x0F00u)	/* b11-8: Status Recovery */
#define	  USBC_STSR_SET			 0x0800u

/* USB Request Type Register */
/*#define	USBC_USBREQ			(*((REGP*)(USBC_BASE+0x54))) */
#define	USBC_BREQUEST			(0xFF00u)	/* b15-8: USBC_BREQUEST */
#define	USBC_BMREQUESTTYPE		(0x00FFu)	/* b7-0: USBC_BMREQUESTTYPE */
#define	USBC_BMREQUESTTYPEDIR	(0x0080u)	/* b7  : Data transfer direction */
#define	USBC_BMREQUESTTYPETYPE	(0x0060u)	/* b6-5: Type */
#define	USBC_BMREQUESTTYPERECIP	(0x001Fu)	/* b4-0: Recipient */

/* USB Request Value Register */
/*#define	USBC_USBVAL			(*((REGP*)(USBC_BASE+0x56))) */
#define	USBC_WVALUE				(0xFFFFu)	/* b15-0: wValue */
#define	USBC_DT_TYPE			(0xFF00u)
#define	USBC_GET_DT_TYPE(v)		(((v) & USBC_DT_TYPE) >> 8)
#define	USBC_DT_INDEX			(0x00FFu)
#define	USBC_CONF_NUM			(0x00FFu)
#define	USBC_ALT_SET			(0x00FFu)

/* USB Request Index Register */
/*#define	USBC_USBINDX		(*((REGP*)(USBC_BASE+0x58))) */
#define	USBC_WINDEX				(0xFFFFu)	/* b15-0: wIndex */
#define	USBC_TEST_SELECT		(0xFF00u)	/* b15-b8: Test Mode Selectors */
#define	  USBC_TEST_J			 0x0100u		/* Test_J */
#define	  USBC_TEST_K			 0x0200u		/* Test_K */
#define	  USBC_TEST_SE0_NAK		 0x0300u		/* Test_SE0_NAK */
#define	  USBC_TEST_PACKET		 0x0400u		/* Test_Packet */
#define	  USBC_TEST_FORCE_ENABLE 0x0500u		/* Test_Force_Enable */
#define	  USBC_TEST_STSelectors	 0x0600u		/* Standard test selectors */
#define	  USBC_TEST_RESERVED	 0x4000u		/* Reserved */
#define	  USBC_TEST_VSTMODES	 0xC000u		/* VendorSpecific test modes */
#define	USBC_EP_DIR				(0x0080u)	/* b7: Endpoint Direction */
#define	  USBC_EP_DIR_IN		 0x0080u
#define	  USBC_EP_DIR_OUT		 0x0000u

/* USB Request Length Register */
/*#define	USBC_USBLENG		(*((REGP*)(USBC_BASE+0x5A))) */
#define	USBC_WLENGTH			(0xFFFFu)	/* b15-0: wLength */


/* Pipe Window Select Register */
/*#define	USBC_PIPESEL		(*((REGP*)(USBC_BASE+0x64))) */
#define	USBC_PIPENM				(0x0007u)	/* b2-0: Pipe select */

/* Default Control Pipe Configuration Register */
/*#define	USBC_DCPCFG			(*((REGP*)(USBC_BASE+0x5C))) */
/* Pipe Configuration Register */
/*#define	USBC_PIPECFG		(*((REGP*)(USBC_BASE+0x68))) */
/* Refer to usbd_DefUSBIP.h */
/*#define	USBC_TYPE			(0xC000u)	/* b15-14: Transfer type */
/*#define	USBC_BFRE			(0x0400u)	/* b10: Buffer ready interrupt
												 mode select */
/*#define	USBC_DBLB			(0x0200u)	/* b9: Double buffer mode select */
/*#define	USBC_CBTMD			(0x0100u)	/* b8: Continuous transfer mode
												 select */
/*#define	USBC_SHTNAK			(0x0080u)	/* b7: Transfer end NAK */
/*#define	USBC_DIR			(0x0010u)	/* b4: Transfer direction select */
/*#define	USBC_EPNUM			(0x000Fu)	/* b3-0: Endpoint number select */

/* Pipe Buffer Configuration Register */
/*#define	USBC_PIPEBUF		(*((REGP*)(USBC_BASE+0x6A))) */
/* Refer to usbd_DefUSBIP.h */
#define	USBC_BUFSIZE			(0x7C00u)	/* b14-10: Pipe buffer size */
#define	USBC_BUFNMB				(0x007Fu)	/* b6-0: Pipe buffer number */
#define	USBC_PIPE0BUF			(256u)
#define	USBC_PIPEXBUF			(64u)

/* Default Control Pipe Maxpacket Size Register */
/* Pipe Maxpacket Size Register */
/*#define	USBC_DCPMAXP		(*((REGP*)(USBC_BASE+0x5E))) */
/* Pipe Maxpacket Size Register */
/*#define	USBC_PIPEMAXP		(*((REGP*)(USBC_BASE+0x6C))) */
#define	USBC_DEVSEL				(0xF000u)	/* b15-14: Device address select */
#define	USBC_MAXP				(0x007Fu)	/* b6-0: Maxpacket size of default
											 control pipe */
#define	USBC_MXPS				(0x07FFu)	/* b10-0: Maxpacket size */

/* Pipe Cycle Configuration Register */
/*#define	USBC_PIPEPERI		(*((REGP*)(USBC_BASE+0x6E))) */
/* Refer to usbd_DefUSBIP.h */
/*#define	USBC_IFIS			(0x1000u)	/* b12: Isochronous in-buffer
												 flash mode select */
/*#define	USBC_IITV			(0x0007u)	/* b2-0: Isochronous interval */

/* Default Control Pipe Control Register */
/*#define	USBC_DCPCTR			(*((REGP*)(USBC_BASE+0x60))) */
/* Pipex Control Register */
/*#define	USBC_PIPE1CTR		(*((REGP*)(USBC_BASE+0x70))) */
/*#define	USBC_PIPE2CTR		(*((REGP*)(USBC_BASE+0x72))) */
/*#define	USBC_PIPE3CTR		(*((REGP*)(USBC_BASE+0x74))) */
/*#define	USBC_PIPE4CTR		(*((REGP*)(USBC_BASE+0x76))) */
/*#define	USBC_PIPE5CTR		(*((REGP*)(USBC_BASE+0x78))) */
/*#define	USBC_PIPE6CTR		(*((REGP*)(USBC_BASE+0x7A))) */
/*#define	USBC_PIPE7CTR		(*((REGP*)(USBC_BASE+0x7C))) */
/*#define	USBC_PIPE8CTR		(*((REGP*)(USBC_BASE+0x7E))) */
/*#define	USBC_PIPE9CTR		(*((REGP*)(USBC_BASE+0x80))) */
#define	USBC_BSTS				(0x8000u)	/* b15: Buffer status */
#define	USBC_SUREQ				(0x4000u)	/* b14: Send USB request  */
#define	USBC_INBUFM				(0x4000u)	/* b14: IN buffer monitor
												(Only for PIPE1 to 5) */
#define	USBC_CSCLR				(0x2000u)	/* b13: c-split status clear */
#define	USBC_CSSTS				(0x1000u)	/* b12: c-split status */
#define	USBC_SUREQCLR			(0x0800u)	/* b11: stop setup request */
#define	USBC_ATREPM				(0x0400u)	/* b10: Auto repeat mode */
#define	USBC_ACLRM				(0x0200u)	/* b9: buffer auto clear mode */
#define	USBC_SQCLR				(0x0100u)	/* b8: Sequence bit clear */
#define	USBC_SQSET				(0x0080u)	/* b7: Sequence bit set */
#define	USBC_SQMON				(0x0040u)	/* b6: Sequence bit monitor */
#define	USBC_PBUSY				(0x0020u)	/* b5: pipe busy */
#define	USBC_PINGE				(0x0010u)	/* b4: ping enable */
#define	USBC_CCPL				(0x0004u)	/* b2: Enable control transfer
											 complete */
#define	USBC_PID				(0x0003u)	/* b1-0: Response PID */
#define	  USBC_PID_STALL		 0x0002u		/* STALL */
#define	  USBC_PID_BUF			 0x0001u		/* BUF */
#define	  USBC_PID_NAK			 0x0000u		/* NAK */


/* PIPExTRE */
/*#define	USBC_PIPE1TRE		(*((REGP*)(USBC_BASE+0x90))) */
/*#define	USBC_PIPE2TRE		(*((REGP*)(USBC_BASE+0x94))) */
/*#define	USBC_PIPE3TRE		(*((REGP*)(USBC_BASE+0x98))) */
/*#define	USBC_PIPE4TRE		(*((REGP*)(USBC_BASE+0x9C))) */
/*#define	USBC_PIPE5TRE		(*((REGP*)(USBC_BASE+0xA0))) */
#define	USBC_TRENB				(0x0200u)	/* b9: Transaction count enable */
#define	USBC_TRCLR				(0x0100u)	/* b8: Transaction count clear */


/* PIPExTRN */
/*#define	USBC_PIPE1TRN		(*((REGP*)(USBC_BASE+0x92))) */
/*#define	USBC_PIPE2TRN		(*((REGP*)(USBC_BASE+0x96))) */
/*#define	USBC_PIPE3TRN		(*((REGP*)(USBC_BASE+0x9A))) */
/*#define	USBC_PIPE4TRN		(*((REGP*)(USBC_BASE+0x9E))) */
/*#define	USBC_PIPE5TRN		(*((REGP*)(USBC_BASE+0xA2))) */
#define	USBC_TRNCNT				(0xFFFFu)	/* b15-0: Transaction counter */


/* DEVADDx */
/*#define	USBC_DEVADD0		(*((REGP*)(USBC_BASE+0xD0))) */
/*#define	USBC_DEVADD1		(*((REGP*)(USBC_BASE+0xD2))) */
/*#define	USBC_DEVADD2		(*((REGP*)(USBC_BASE+0xD4))) */
/*#define	USBC_DEVADD3		(*((REGP*)(USBC_BASE+0xD6))) */
/*#define	USBC_DEVADD4		(*((REGP*)(USBC_BASE+0xD8))) */
/*#define	USBC_DEVADD5		(*((REGP*)(USBC_BASE+0xDA))) */
/*#define	USBC_DEVADD6		(*((REGP*)(USBC_BASE+0xDC))) */
/*#define	USBC_DEVADD7		(*((REGP*)(USBC_BASE+0xDE))) */
/*#define	USBC_DEVADD8		(*((REGP*)(USBC_BASE+0xE0))) */
/*#define	USBC_DEVADD9		(*((REGP*)(USBC_BASE+0xE2))) */
/*#define	USBC_DEVADDA		(*((REGP*)(USBC_BASE+0xE4))) */
#define	USBC_UPPHUB				(0x7800u)
#define	USBC_HUBPORT			(0x0700u)
#define	USBC_USBSPD				(0x00C0u)
#define	USBC_RTPORT				(0x0001u)


/* Invalid Register. for CS-return from PCUT */
/*#pragma	USBC_ADDRESS		INVALID_REG	(_USBBASE + EEh) */

#endif	/* __R_USBC_CUSBDEFBITDEFINE_H__ */
/******************************************************************************
End  Of File
******************************************************************************/
