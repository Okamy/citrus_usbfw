/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_pStdRequest.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Peripheral standard request code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW global definition */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _pcd


/*****************************************************************************
Public Variables
******************************************************************************/
extern	uint16_t		usb_gpstd_StallPipe[];
extern	USBC_CB_INFO_t	usb_gpstd_StallCB;

/******************************************************************************
Renesas Abstracted Peripheral standard request functions
******************************************************************************/

/******************************************************************************
Function Name   : usb_pstd_StandReq0
Description     : Standard request (idle or setup stage)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_StandReq0(void)
{
	switch( usb_gpstd_ReqRequest )
	{
	case USBC_CLEAR_FEATURE:
			/* Clear Feature0 */
			usb_pstd_ClearFeature0();
			break;
	case USBC_SET_FEATURE:
			/* Set Feature0 */
			usb_pstd_SetFeature0();
			break;
	case USBC_SET_ADDRESS:
			/* Set Address0 */
			usb_pstd_SetAddress0();
			break;
	case USBC_SET_CONFIGURATION:
			/* Set Configuration0 */
			usb_pstd_SetConfiguration0();
			break;
	case USBC_SET_INTERFACE:
			/* Set Interface0 */
			usb_pstd_SetInterface0();
			break;
	default:
			break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_StandReq1
Description     : Standard request (control read data stage)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_StandReq1(void)
{
	switch( usb_gpstd_ReqRequest )
	{
	case USBC_GET_STATUS:
			/* Get Status1 */
			usb_pstd_GetStatus1();
			break;
	case USBC_GET_DESCRIPTOR:
			/* Get Descriptor1 */
			usb_pstd_GetDescriptor1();
			break;
	case USBC_GET_CONFIGURATION:
			/* Get Configuration1 */
			usb_pstd_GetConfiguration1();
			break;
	case USBC_GET_INTERFACE:
			/* Get Interface1 */
			usb_pstd_GetInterface1();
			break;
	case USBC_SYNCH_FRAME:
			/* Synch Frame */
			usb_pstd_SynchFrame1();
			break;
	default:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_StandReq2
Description     : Standard Request (control write data stage)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_StandReq2(void)
{
	if( usb_gpstd_ReqRequest == USBC_SET_DESCRIPTOR )
	{
		/* Set Descriptor2 */
		usb_pstd_SetDescriptor2();
	}
	else
	{
		/* Set pipe USBC_PID_STALL */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_StandReq3
Description     : Standard request (control write nodata status stage)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_StandReq3(void)
{
	switch( usb_gpstd_ReqRequest )
	{
	case USBC_CLEAR_FEATURE:
			/* ClearFeature3 */
			usb_pstd_ClearFeature3();
			break;
	case USBC_SET_FEATURE:
			/* SetFeature3 */
			usb_pstd_SetFeature3();
			break;
	case USBC_SET_ADDRESS:
			/* SetAddress3 */
			usb_pstd_SetAddress3();
			break;
	case USBC_SET_CONFIGURATION:
			/* SetConfiguration3 */
			usb_pstd_SetConfiguration3();
			break;
	case USBC_SET_INTERFACE:
			/* SetInterface3 */
			usb_pstd_SetInterface3();
			break;
	default:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
	}
	/* Control transfer stop(end) */
	R_usb_pstd_ControlEnd((uint16_t)USBC_CTRL_END);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_StandReq4
Description     : Standard request (control read status stage)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_StandReq4(void)
{
	switch( usb_gpstd_ReqRequest )
	{
	case USBC_GET_STATUS:
			/* GetStatus4 */
			usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			break;
	case USBC_GET_DESCRIPTOR:
			/* GetDescriptor4 */
			usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			break;
	case USBC_GET_CONFIGURATION:
			/* GetConfiguration4 */
			usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			break;
	case USBC_GET_INTERFACE:
			/* GetInterface4 */
			usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			break;
	case USBC_SYNCH_FRAME:
			/* SynchFrame4 */
			usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			break;
	default:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
	}
	/* Control transfer stop(end) */
	R_usb_pstd_ControlEnd((uint16_t)USBC_CTRL_END);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_StandReq5
Description     : Standard request (control write status stage)
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_StandReq5(void)
{
	if( usb_gpstd_ReqRequest == USBC_SET_DESCRIPTOR )
	{
		/* Set pipe PID_BUF */
		usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
	}
	else
	{
		/* Set pipe USBC_PID_STALL */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
	/* Control transfer stop(end) */
	R_usb_pstd_ControlEnd((uint16_t)USBC_CTRL_END);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_GetStatus1
Description     : Get Status1
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_GetStatus1(void)
{
	static uint8_t		tbl[2];
	uint16_t			ep;
	uint16_t			buffer, pipe;

	if( (usb_gpstd_ReqValue == 0) && (usb_gpstd_ReqLength == 2) )
	{
		tbl[0] = 0;
		tbl[1] = 0;
		/* check Request type */
		switch( usb_gpstd_ReqTypeRecip )
		{
		case USBC_DEVICE:
			if( usb_gpstd_ReqIndex == 0 )
			{
				/* Self powered / Bus powered */
				tbl[0] = usb_pstd_GetCurrentPower();
				/* Support remote wakeup ? */
				if( usb_gpstd_RemoteWakeup == USBC_YES )
				{
					tbl[0] |= USBC_GS_REMOTEWAKEUP;
				}
				/* Control read start */
				R_usb_pstd_ControlRead((uint32_t)2, tbl);
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		case USBC_INTERFACE:
			if( usb_pstd_ChkConfigured() == USBC_YES )
			{
				if( usb_gpstd_ReqIndex 
					< usb_pstd_GetInterfaceNum(usb_gpstd_ConfigNum) )
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)2, tbl);
				}
				else
				{
					/* Request error (not exist interface) */
					usb_cstd_SetStall((uint16_t)USBC_PIPE0);
				}
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		case USBC_ENDPOINT:
			/* Endpoint number */
			ep = (uint16_t)(usb_gpstd_ReqIndex & USBC_EPNUMFIELD);
			/* Endpoint 0 */
			if( ep == 0 )
			{
				USB_RD(DCPCTR, buffer);
				if( (buffer & USBC_PID_STALL) != (uint16_t)0 )
				{
				   /* Halt set */
				   tbl[0] = USBC_GS_HALT;
				}
				/* Control read start */
				R_usb_pstd_ControlRead((uint32_t)2, tbl);
			}
			/* EP1 to max */
			else if( ep <= USBC_MAX_EP_NO )
			{
				if( usb_pstd_ChkConfigured() == USBC_YES )
				{
					pipe = usb_cstd_Epadr2Pipe(usb_gpstd_ReqIndex);
					if( pipe == USBC_ERROR )
					{
						/* Set pipe USBC_PID_STALL */
						usb_cstd_SetStall((uint16_t)USBC_PIPE0);
					}
					else
					{
						buffer = usb_cstd_GetPid(pipe);
						if( (buffer & USBC_PID_STALL) != (uint16_t)0 )
						{
							/* Halt set */
							tbl[0] = USBC_GS_HALT;
						}
						/* Control read start */
						R_usb_pstd_ControlRead((uint32_t)2, tbl);
					}
				}
				else
				{
					/* Set pipe USBC_PID_STALL */
					usb_cstd_SetStall((uint16_t)USBC_PIPE0);
				}
			}
			else
			{
				/* Set pipe USBC_PID_STALL */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		default:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		}
	}
	else
	{
		/* Set pipe USBC_PID_STALL */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_GetDescriptor1
Description     : Get Descriptor1
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_GetDescriptor1(void)
{
	uint16_t		len;
	uint16_t		idx;
	uint8_t			*table;

	if( usb_gpstd_ReqTypeRecip == USBC_DEVICE )
	{
		idx = (uint16_t)(usb_gpstd_ReqValue & USBC_DT_INDEX);
		switch( (uint16_t)USBC_GET_DT_TYPE(usb_gpstd_ReqValue) )
		{
		/*---- Device descriptor ----*/
		case USBC_DT_DEVICE:
			if((usb_gpstd_ReqIndex == (uint16_t)0) && (idx == (uint16_t)0))
			{
				table = usb_gpstd_Driver.devicetbl;
				if( usb_gpstd_ReqLength < table[0] )
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)usb_gpstd_ReqLength
						, table);
				}
				else
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)table[0], table);
				}
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		/*---- Configuration descriptor ----*/
		case USBC_DT_CONFIGURATION:
			if( usb_gpstd_ReqIndex == 0 )
			{
				table = usb_gpstd_Driver.configtbl[idx];
				len	 = (uint16_t)(*(uint8_t*)((uint32_t)table
					+ (uint32_t)3));
				len	 = (uint16_t)(len << 8);
				len += (uint16_t)(*(uint8_t*)((uint32_t)table
					+ (uint32_t)2));
				/* Descriptor > wLength */
				if( usb_gpstd_ReqLength < len )
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)usb_gpstd_ReqLength
						, table);
				}
				else
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)len, table);
				}
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		/*---- String descriptor ----*/
		case USBC_DT_STRING:
			if( idx <= USB_STRINGNUM )
			{
				table = usb_gpstd_Driver.stringtbl[idx];
				len	 = (uint16_t)(*(uint8_t*)((uint32_t)table
					+ (uint32_t)0));
				if( usb_gpstd_ReqLength < len )
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)usb_gpstd_ReqLength
						, table);
				}
				else
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)len, table);
				}
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		/*---- Interface descriptor ----*/
		case USBC_DT_INTERFACE:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		/*---- Endpoint descriptor ----*/
		case USBC_DT_ENDPOINT:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		case USBC_DT_DEVICE_QUALIFIER:
			if( ((usb_cstd_HiSpeedEnable((uint16_t)USBC_PORT0) == USBC_YES)
				&& (usb_gpstd_ReqIndex == 0)) && (idx == 0) )
			{
				table = usb_gpstd_Driver.qualitbl;
				if( usb_gpstd_ReqLength < table[0] )
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)usb_gpstd_ReqLength
						, table);
				}
				else
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)table[0], table);
				}
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		case USBC_DT_OTHER_SPEED_CONF:
			if( (usb_cstd_HiSpeedEnable((uint16_t)USBC_PORT0) == USBC_YES)
				&& (usb_gpstd_ReqIndex == 0) )
			{
				table = usb_gpstd_Driver.othertbl[idx];
				len	 = (uint16_t)(*(uint8_t*)((uint32_t)table
					+ (uint32_t)3));
				len	 = (uint16_t)(len << 8);
				len += (uint16_t)(*(uint8_t*)((uint32_t)table
					+ (uint32_t)2));
				/* Descriptor > wLength */
				if( usb_gpstd_ReqLength < len )
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)usb_gpstd_ReqLength
						, table);
				}
				else
				{
					/* Control read start */
					R_usb_pstd_ControlRead((uint32_t)len, table);
				}
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		case USBC_DT_INTERFACE_POWER:
			/* Not support */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		default:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		}
	}
	else if( usb_gpstd_ReqTypeRecip == USBC_INTERFACE )
	{
		usb_gpstd_ReqReg.ReqType		= usb_gpstd_ReqType;
		usb_gpstd_ReqReg.ReqTypeType	= usb_gpstd_ReqTypeType;
		usb_gpstd_ReqReg.ReqTypeRecip	= usb_gpstd_ReqTypeRecip;
		usb_gpstd_ReqReg.ReqRequest		= usb_gpstd_ReqRequest;
		usb_gpstd_ReqReg.ReqValue		= usb_gpstd_ReqValue;
		usb_gpstd_ReqReg.ReqIndex		= usb_gpstd_ReqIndex;
		usb_gpstd_ReqReg.ReqLength		= usb_gpstd_ReqLength;
		usb_cstd_GetDescriptorInterface( &usb_gpstd_ReqReg );
	}
	else
	{
		/* Set pipe USBC_PID_STALL */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_GetConfiguration1
Description     : Get Configuration1
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_GetConfiguration1(void)
{
	static uint8_t	tbl[2];

	/* check request */
	if( (((usb_gpstd_ReqTypeRecip == USBC_DEVICE) 
		&& (usb_gpstd_ReqValue == 0)) 
		&& (usb_gpstd_ReqIndex == 0)) 
		&& (usb_gpstd_ReqLength == 1) )
	{
		tbl[0] = (uint8_t)usb_gpstd_ConfigNum;
		/* Control read start */
		R_usb_pstd_ControlRead((uint32_t)1, tbl);
	}
	else
	{
		/* Set pipe USBC_PID_STALL */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}


/******************************************************************************
Function Name   : usb_pstd_GetInterface1
Description     : Get Interface1
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_GetInterface1(void)
{
	static uint8_t	tbl[2];

	/* check request */
	if( ((usb_gpstd_ReqTypeRecip == USBC_INTERFACE) 
		&& (usb_gpstd_ReqValue == 0)) && (usb_gpstd_ReqLength == 1) )
	{
		if( usb_gpstd_ReqIndex < USBC_ALT_NO )
		{
			tbl[0] = (uint8_t)usb_gpstd_AltNum[usb_gpstd_ReqIndex];
			/* Start control read */
			R_usb_pstd_ControlRead((uint32_t)1, tbl);
		}
		else
		{
			/* Request error */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
		}
	}
	else
	{
		/* Request error */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClearFeature0
Description     : Clear Feature0
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ClearFeature0(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_ClearFeature3
Description     : Clear Feature3
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_ClearFeature3(void)
{
	uint16_t		pipe;
	uint16_t		ep;

	if( usb_gpstd_ReqLength == 0 )
	{
		/* check request type */
		switch( usb_gpstd_ReqTypeRecip )
		{
		case USBC_DEVICE:
			if( (usb_gpstd_ReqValue == USBC_DEV_REMOTE_WAKEUP)
				&& (usb_gpstd_ReqIndex == 0) )
			{
				if( usb_pstd_ChkRemote() == USBC_YES )
				{
					usb_gpstd_RemoteWakeup = USBC_NO;
					/* Set pipe PID_BUF */
					usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
				}
				else
				{
					/* Not support remote wakeup */
					/* Request error */
					usb_cstd_SetStall((uint16_t)USBC_PIPE0);
				}
			}
			else
			{
				/* Not specification */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		case USBC_INTERFACE:
			/* Request error */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		case USBC_ENDPOINT:
			/* Endpoint number */
			ep = (uint16_t)(usb_gpstd_ReqIndex & USBC_EPNUMFIELD);
			if( usb_gpstd_ReqValue == USBC_ENDPOINT_HALT )
			{
				/* EP0 */
				if( ep == 0 )
				{
					/* Stall clear */
					usb_cstd_ClrStall((uint16_t)USBC_PIPE0);
					/* Set pipe PID_BUF */
					usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
				}
				/* EP1 to max */
				else if( ep <= USBC_MAX_EP_NO )
				{
					pipe = usb_cstd_Epadr2Pipe(usb_gpstd_ReqIndex);
					if( pipe == USBC_ERROR )
					{
						/* Request error */
						usb_cstd_SetStall((uint16_t)USBC_PIPE0);
					}
					else
					{
						if( usb_cstd_GetPid(pipe) == USBC_PID_BUF )
						{
							usb_cstd_ClrStall(pipe);
							/* SQCLR=1 */
							usb_cstd_DoSqclr(pipe);
							/* Set pipe PID_BUF */
							usb_cstd_SetBuf( pipe);
						}
						else
						{
							usb_cstd_ClrStall(pipe);
							/* SQCLR=1 */
							usb_cstd_DoSqclr(pipe);
						}
						/* Set pipe PID_BUF */
						usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
						if( usb_gpstd_StallPipe[pipe] == USBC_YES )
						{
							usb_gpstd_StallPipe[pipe] = USBC_DONE;
							(*usb_gpstd_StallCB)(pipe, (uint16_t)0);
						}
					}
				}
				else
				{
					/* Request error */
					usb_cstd_SetStall((uint16_t)USBC_PIPE0);
				}
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;
		default:
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		}
	}
	else
	{
		/* Not specification */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetFeature0
Description     : Set Feature0
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetFeature0(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetFeature3
Description     : Set Feature3
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetFeature3(void)
{
	uint16_t		pipe;
	uint16_t		ep;

	if( usb_gpstd_ReqLength == 0 )
	{
		/* check request type */
		switch( usb_gpstd_ReqTypeRecip )
		{
		case USBC_DEVICE:
			switch( usb_gpstd_ReqValue )
			{
			case USBC_DEV_REMOTE_WAKEUP:
				if( usb_gpstd_ReqIndex == 0 )
				{
					if( usb_pstd_ChkRemote() == USBC_YES )
					{
						usb_gpstd_RemoteWakeup = USBC_YES;
						/* Set pipe PID_BUF */
						usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
					}
					else
					{
						/* Not support remote wakeup */
						/* Request error */
						usb_cstd_SetStall((uint16_t)USBC_PIPE0);
					}
				}
				else
				{
					/* Not specification */
					usb_cstd_SetStall((uint16_t)USBC_PIPE0);
				}
				break;
			case USBC_TEST_MODE:
				if( usb_cstd_PortSpeed((uint16_t)USBC_PORT0)
					== USBC_HSCONNECT )
				{
					if( (usb_gpstd_ReqIndex < USBC_TEST_RESERVED) 
						|| (USBC_TEST_VSTMODES <= usb_gpstd_ReqIndex) )
					{
						usb_gpstd_TestModeFlag = USBC_YES;
						usb_gpstd_TestModeSelect = usb_gpstd_ReqIndex;
						/* Set pipe PID_BUF */
						usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
					}
					else
					{
						/* Not specification */
						usb_cstd_SetStall((uint16_t)USBC_PIPE0);
					}
				}
				else
				{
					/* Not specification */
					usb_cstd_SetStall((uint16_t)USBC_PIPE0);
				}
				break;
			default:
				usb_pstd_SetFeatureFunction();
				break;
			}
			break;
		case USBC_INTERFACE:
			/* Set pipe USBC_PID_STALL */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		case USBC_ENDPOINT:
			/* Endpoint number */
			ep = (uint16_t)(usb_gpstd_ReqIndex & USBC_EPNUMFIELD);
			if( usb_gpstd_ReqValue == USBC_ENDPOINT_HALT )
			{
				/* EP0 */
				if( ep == 0 )
				{
					/* Set pipe PID_BUF */
					usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
				}
				/* EP1 to max */
				else if( ep <= USBC_MAX_EP_NO )
				{
					pipe = usb_cstd_Epadr2Pipe(usb_gpstd_ReqIndex);
					if( pipe == USBC_ERROR )
					{
						/* Request error */
						usb_cstd_SetStall((uint16_t)USBC_PIPE0);
					}
					else
					{
						/* Set pipe USBC_PID_STALL */
						usb_cstd_SetStall(pipe);
						/* Set pipe PID_BUF */
						usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
					}
				}
				else
				{
					/* Request error */
					usb_cstd_SetStall((uint16_t)USBC_PIPE0);
				}
			}
			else
			{
				/* Not specification */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
			break;

		default:
			/* Request error */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			break;
		}
	}
	else
	{
		/* Request error */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetAddress0
Description     : Set Address0
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetAddress0(void)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetAddress3
Description     : Set Address3
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetAddress3(void)
{
	if( usb_gpstd_ReqTypeRecip == USBC_DEVICE )
	{
		if( (usb_gpstd_ReqIndex == 0) && (usb_gpstd_ReqLength == 0) )
		{
			if( usb_gpstd_ReqValue <= 127 )
			{
				/* Set pipe PID_BUF */
				usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
			}
			else
			{
				/* Not specification */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
		}
		else
		{
			/* Not specification */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
		}
	}
	else
	{
		/* Request error */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetDescriptor2
Description     : Set Descriptor2
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetDescriptor2(void)
{
	/* Not specification */
	usb_cstd_SetStall((uint16_t)USBC_PIPE0);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetConfiguration0
Description     : Set Configuration0
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetConfiguration0(void)
{
	if( usb_gpstd_ConfigNum > 0 )
	{
		/* Registration open function call */
		(*usb_gpstd_Driver.devconfig)(usb_gpstd_ConfigNum
			, (uint16_t)USBC_NO_ARG);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetConfiguration3
Description     : Set Configuration3
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetConfiguration3(void)
{
	uint16_t		i, j;
	uint16_t		ifc, cfgnum, cfgok;
	uint16_t		*table;
	uint8_t			*table2;

	if( usb_gpstd_ReqTypeRecip == USBC_DEVICE )
	{
		cfgnum	= usb_pstd_GetConfigNum();
		cfgok	= USBC_NG;

		for ( j = 0; j < cfgnum; j++ )
		{
			table2 = usb_gpstd_Driver.configtbl[j];

			if( (((usb_gpstd_ReqValue == table2[5])
				|| (usb_gpstd_ReqValue == 0))
				&& (usb_gpstd_ReqIndex == 0)) 
				&& (usb_gpstd_ReqLength == 0) )
			{
				/* Configuration number set */
				usb_pstd_SetConfigNum(usb_gpstd_ReqValue);
				usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
				cfgok	= USBC_OK;
				if( usb_gpstd_ConfigNum > 0 )
				{
					usb_pstd_ClearEpTblIndex();
					ifc = usb_pstd_GetInterfaceNum(usb_gpstd_ConfigNum);
					for( i = 0; i < ifc; ++i )
					{
						/* Endpoint table initialize */
						usb_pstd_SetEpTblIndex(usb_gpstd_ConfigNum
							, i, (uint16_t)0);
					}
					table = usb_gpstd_Driver.pipetbl[usb_gpstd_ConfigNum - 1];
					/* Set pipe configuration register */
					R_usb_pstd_SetPipeRegister((uint16_t)USBC_PERIPIPE, table);
				}
				break;
			}
		}
		if( cfgok == USBC_NG )
		{
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
		}
	}
	else
	{
		/* Request error */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetInterface0
Description     : Set Interface0
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetInterface0(void)
{
	/* Interfaced change function call */
	(*usb_gpstd_Driver.interface)(usb_gpstd_AltNum[usb_gpstd_ReqIndex]
		, (uint16_t)USBC_NO_ARG);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SetInterface3
Description     : Set Interface3
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SetInterface3(void)
{
	uint16_t		*table;
	uint16_t		conf;

	conf = usb_gpstd_ConfigNum;
	if( conf < (uint16_t)1 )
	{
		/* Address state */
		conf = (uint16_t)1;
	}

	/* Configured ? */
	if( (usb_pstd_ChkConfigured() == USBC_YES) 
		&& (usb_gpstd_ReqTypeRecip == USBC_INTERFACE) )
	{
		if( (usb_gpstd_ReqIndex 
			<= usb_pstd_GetInterfaceNum(usb_gpstd_ConfigNum)) 
				&& (usb_gpstd_ReqLength == 0) )
		{
			if( usb_gpstd_ReqValue 
				<= usb_pstd_GetAlternateNum(usb_gpstd_ConfigNum
					, usb_gpstd_ReqIndex) )
			{
				usb_gpstd_AltNum[usb_gpstd_ReqIndex] 
					= (uint16_t)(usb_gpstd_ReqValue & USBC_ALT_SET);
				usb_cstd_SetBuf((uint16_t)USBC_PIPE0);
				usb_pstd_ClearEpTblIndex();
				/* Search endpoint setting */
				usb_pstd_SetEpTblIndex(usb_gpstd_ConfigNum
					, usb_gpstd_ReqIndex
					, usb_gpstd_AltNum[usb_gpstd_ReqIndex]);
				table = usb_gpstd_Driver.pipetbl[conf - 1];
				/* Set pipe configuration register */
				R_usb_pstd_SetPipeRegister((uint16_t)USBC_PERIPIPE, table);
			}
			else
			{
				/* Request error */
				usb_cstd_SetStall((uint16_t)USBC_PIPE0);
			}
		}
		else
		{
			/* Request error */
			usb_cstd_SetStall((uint16_t)USBC_PIPE0);
		}
	}
	else
	{
		/* Request error */
		usb_cstd_SetStall((uint16_t)USBC_PIPE0);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_pstd_SynchFrame1
Description     : Synch Frame
Arguments       : none
Return value    : none
******************************************************************************/
void usb_pstd_SynchFrame1(void)
{
	/* Set pipe USBC_PID_STALL */
	usb_cstd_SetStall((uint16_t)USBC_PIPE0);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
End  Of File
******************************************************************************/
