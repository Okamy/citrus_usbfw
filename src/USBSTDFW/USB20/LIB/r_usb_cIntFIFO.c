/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cIntFIFO.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral Interrupt code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usbc_cMacSystemcall.h"	/* System call macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _usblib

/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_BrdyPipe
Description     : BRDY interrupt
Arguments       : uint16_t bitsts       ; BRDYSTS Register & BRDYENB Register
Return value    : none
******************************************************************************/
void usb_cstd_BrdyPipe(uint16_t bitsts)
{
	uint16_t		useport, buffer, i;
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	uint16_t		tmp_data_cnt;
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */

	for( i = USBC_PIPE1; i <= USBC_MAX_PIPE_NO; i++ )
	{
		if( (bitsts & USBC_BITSET(i)) != 0 )
		{
			/* Interrupt check */
			USB_CLR_STS(BEMPSTS, USBC_BITSET(i));
			if( usb_gcstd_Pipe[i] != USBC_NULL )
			{
				/* Pipe number to FIFO port select */
				useport = usb_cstd_Pipe2Fport(i);
				if( useport == USBC_D0DMA )
				{
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
					/* DMA Transfer request disable */
					USB_CLR_PAT(D0FIFOSEL, USBC_DREQE);
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
				
					/* DMA stop */
					usb_cstd_StopDma();
				
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
					/* Changes FIFO port by the pipe. */
					buffer = usb_cstd_FPortChange1(i, useport, USBC_NO);
					/* Get D0fifo Receive Data Length */
					usb_gcstd_Dma0Size = (uint16_t)(buffer & USBC_DTLN);
					USBC_PRINTF2("*** DMA receive short %d (%d)\n"
						, usb_gcstd_DataCnt[i], usb_gcstd_Dma0Size);

					if((usb_gcstd_DataCnt[i] % usb_gcstd_Dma0Fifo) == 0)
					{
						tmp_data_cnt = usb_gcstd_Dma0Fifo;
					}
					else
					{
						tmp_data_cnt = usb_gcstd_DataCnt[i] % usb_gcstd_Dma0Fifo;
					}

					usb_gcstd_DataCnt[i] = 0;

					/* Check data count */
					if( usb_gcstd_Dma0Size >= tmp_data_cnt )
#else	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
					/* Check data count */
					if( usb_gcstd_DataCnt[i] == (uint32_t)0u )
#endif	/* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
					{
						/* End of data transfer */
						usb_cstd_DataEnd(i, (uint16_t)USBC_DATA_OK);
					}
					else
					{
#if USBC_TARGET_CHIP_PP != USBC_RX600_PP
						/* Changes FIFO port by the pipe. */
						buffer = usb_cstd_FPortChange1(i, useport, USBC_NO);
						usb_gcstd_Dma0Size = (uint16_t)(buffer & USBC_DTLN);
						USBC_PRINTF2("*** DMA receive short %d (%d)\n"
							, usb_gcstd_DataCnt[i], usb_gcstd_Dma0Size);
#endif /* USBC_TARGET_CHIP_PP != USBC_RX600_PP */
						/* D0FIFO access DMA stop */
						usb_cstd_D0fifoStopUsb();
						/* End of data transfer */
						usb_cstd_DataEnd(i, (uint16_t)USBC_DATA_SHT);
					}
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
					/* Set BCLR */
					USB_WR(D0FIFOCTR, USBC_BCLR);
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
				}
				else
				{
					if( usb_cstd_GetPipeDir(i) == USBC_BUF2FIFO )
					{
						/* Buffer to FIFO data write */
						usb_cstd_Buf2Fifo(i, useport);
					}
					else
					{
						/* FIFO to Buffer data read */
						usb_cstd_Fifo2Buf(i, useport);
					}
				}
			}
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_NrdyPipe
Description     : Nrdy Pipe interrupt
Arguments       : uint16_t bitsts       ; NRDYSTS Register & NRDYENB Register
Return value    : none
******************************************************************************/
void usb_cstd_NrdyPipe(uint16_t bitsts)
{
/* PERI spec															*/
/*	Transmitting pipe													*/
/* @5		a) only NRDY												*/
/* @1		b) NRDY+OVRN	(Isochronous)								*/
/*	Receive pipe														*/
/* @5		a) only NRDY												*/
/* @1		b) NRDY+OVRN	(Isochronous)								*/
/* @2		c) only NRDY	(interval error of isochronous)				*/
/* HOST spec															*/
/*	Transmitting pipe													*/
/* @1		a) NRDY+OVRN	(Isochronous)								*/
/* @4		b) NRDY+NAK		(Ignore)									*/
/* @3		c) NRDY+STALL	(Receive STALL)								*/
/*	Receive pipe														*/
/* @1		a) NRDY+OVRN	(Isochronous)								*/
/* @4		b) NRDY+NAK		(Ignore)									*/
/* @2		c) NRDY			(Ignore of isochronous)						*/
/* @2		d) NRDY			(CRC error of isochronous)					*/
/* @3		e) NRDY+STALL	(Receive STALL)								*/

	uint16_t		buffer, i;

	for( i = USBC_PIPE1; i <= USBC_MAX_PIPE_NO; i++ )
	{
		if( (bitsts & USBC_BITSET(i)) != 0 )
		{
			/* Interrupt check */
			if( usb_gcstd_Pipe[i] != USBC_NULL )
			{
				if( usb_cstd_GetPipeType(i) == USBC_ISO )
				{
					/* Wait for About 60ns */
					USB_RD(FRMNUM, buffer);
					if( (buffer & USBC_OVRN) == USBC_OVRN )
					{
						/* @1 */
						/* End of data transfer */
						usb_cstd_ForcedTermination(i
							, (uint16_t)USBC_DATA_OVR);
						USBC_PRINTF1("###ISO OVRN %d\n"
							, usb_gcstd_DataCnt[i]);
					}
					else
					{
						/* @2 */
						/* End of data transfer */
						usb_cstd_ForcedTermination(i
							, (uint16_t)USBC_DATA_ERR);
					}
				}
				else
				{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
					buffer = usb_cstd_GetPid(i);
					/* STALL ? */
					if( (buffer & USBC_PID_STALL) == USBC_PID_STALL )
					{
						USBC_PRINTF1("### STALL Pipe %d\n", i);
						/* @4 */
						/* End of data transfer */
						usb_cstd_ForcedTermination(i
							, (uint16_t)USBC_DATA_STALL);
					}
					else
					{
						/* Wait for About 60ns */
						USB_RD(SYSSTS0, buffer);
						/* @3 */
						usb_ghstd_IgnoreCnt[i]++;
						USBC_PRINTF2("### IGNORE Pipe %d is %d times \n"
							, i, usb_ghstd_IgnoreCnt[i]);
						if( usb_ghstd_IgnoreCnt[i] == USBC_PIPEERROR )
						{
							/* Data Device Ignore X 3 call back */
							/* End of data transfer */
							usb_cstd_ForcedTermination(i
								, (uint16_t)USBC_DATA_ERR);
						}
						else
						{
							/* 5ms wait */
							usbc_cpu_DelayXms((uint16_t)5);
							/* PIPEx Data Retry */
							usb_cstd_SetBuf(i);
						}
					}
#endif	/* USBC_HOST_PP */

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
					/* do nothing */
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
					/* Peripheral Function */
					if( usb_cstd_FunctionUsbip() == USBC_NO )
					{
						/* @5 */
					}
					else
					{
						buffer = usb_cstd_GetPid(i);
						/* STALL ? */
						if( (buffer & USBC_PID_STALL) == USBC_PID_STALL )
						{
							USBC_PRINTF1("### STALL Pipe %d\n", i);
							/* @4 */
							/* End of data transfer */
							usb_cstd_ForcedTermination(i, USBC_DATA_STALL);
						}
						else
						{
							/* Wait for About 60ns */
							USB_RD(SYSSTS0, buffer);
							/* @3 */
							usb_ghstd_IgnoreCnt[i]++;
							USBC_PRINTF2("### IGNORE Pipe %d is %d times \n"
								, i, usb_ghstd_IgnoreCnt[i]);
							if( usb_ghstd_IgnoreCnt[i] == USBC_PIPEERROR )
							{
								/* Data Device Ignore X 3 call back */
								/* End of data transfer */
								usb_cstd_ForcedTermination(i
									, USBC_DATA_ERR);
							}
							else
							{
								/* 5ms wait */
								usbc_cpu_DelayXms(5);
								/* PIPEx Data Retry */
								usb_cstd_SetBuf(i);
							}
						}
					}
#endif	/* USBC_HOST_PERI_PP */
				}
			}
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_BempPipe
Description     : BEMP interrupt
Arguments       : uint16_t bitsts       ; BEMPSTS Register & BEMPENB Register
Return value    : none
******************************************************************************/
void usb_cstd_BempPipe(uint16_t bitsts)
{
	uint16_t		buffer, i;
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	uint16_t		useport;
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */

	for( i = USBC_PIPE1; i <= USBC_PIPE5; i++ )
	{
		if( (bitsts & USBC_BITSET(i)) != 0 )
		{
			/* Interrupt check */
			if( usb_gcstd_Pipe[i] != USBC_NULL )
			{
				buffer = usb_cstd_GetPid(i);
				/* MAX packet size error ? */
				if( (buffer & USBC_PID_STALL) == USBC_PID_STALL )
				{
					USBC_PRINTF1("### STALL Pipe %d\n", i);
					usb_cstd_ForcedTermination(i
						, (uint16_t)USBC_DATA_STALL);
				}
				else
				{
					if( usb_cstd_ReadInbufm(i) != USBC_INBUFM )
					{
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
						/* Pipe number to FIFO port select */
						useport = usb_cstd_Pipe2Fport(i);
						if( useport == USBC_D0DMA )
						{
							USB_CLR_PAT(D0FIFOSEL, USBC_DREQE);
						
							/* DMA stop */
							usb_cstd_StopDma();
						
							USB_CLR_STS(BEMPSTS, USBC_BITSET(i));
						}
#endif /* USBC_TARGET_CHIP_PP == USBC_RX600_PP */
						/* End of data transfer */
						usb_cstd_DataEnd(i, (uint16_t)USBC_DATA_NONE);
					}
				}
			}
		}
	}
	for( i = USBC_PIPE6; i <= USBC_MAX_PIPE_NO; i++ )
	{
		/* Interrupt check */
		if( (bitsts & USBC_BITSET(i)) != 0 )
		{
			if( usb_gcstd_Pipe[i] != USBC_NULL )
			{
				buffer = usb_cstd_GetPid(i);
				/* MAX packet size error ? */
				if( (buffer & USBC_PID_STALL) == USBC_PID_STALL )
				{
					USBC_PRINTF1("### STALL Pipe %d\n", i);
					usb_cstd_ForcedTermination(i
						, (uint16_t)USBC_DATA_STALL);
				}
				else
				{
					/* End of data transfer */
					usb_cstd_DataEnd(i, (uint16_t)USBC_DATA_NONE);
				}
			}
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/

/******************************************************************************
End  Of File
******************************************************************************/
