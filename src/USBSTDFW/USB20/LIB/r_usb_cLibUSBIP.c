/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is  protected
* under all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR  A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE  EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE  LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
*******************************************************************************
* Copyright (C) 2010(2011) Renesas Electronics Corpration
* and Renesas Solutions Corp. All rights reserved.
*******************************************************************************
* File Name    : r_usb_cLibUSBIP.c
* Version      : 1.10
* Device(s)    : Renesas SH-Series, RX-Series
* Tool-Chain   : Renesas SuperH RISC engine Standard Toolchain
*              : Renesas RX Standard Toolchain
* OS           : Common to None and uITRON 4.0 Spec
* H/W Platform : Independent
* Description  : USB Host and Peripheral IP Library code
*******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 17.03.2010 0.80    First Release
*         : 30.07.2010 0.90    Updated comments
*         : 02.08.2010 0.91    Updated comments
*         : 29.10.2010 1.00    Mass Production Release
*         : 01.06.2011 1.10    Version 1.10 Release
******************************************************************************/


/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_usbc_cTypedef.h"		/* Type define */
#include "r_usb_cDefUsr.h"			/* USB-H/W register set (user define) */
#include "r_usbc_cDefUSBIP.h"		/* USB-FW Library Header */
#include "r_usbc_cMacPrint.h"		/* Standard IO macro */
#include "r_usb_cExtern.h"			/* USB-FW global define */


/******************************************************************************
Section    <Section Definition> , "Project Sections"
******************************************************************************/
#pragma section _usblib


/******************************************************************************
Constant macro definitions
******************************************************************************/


/******************************************************************************
External variables and functions
******************************************************************************/


/******************************************************************************
Private global variables and functions
******************************************************************************/
/* USB register define */
volatile struct	USBC_REGISTER	*usb_gcstd_UsbReg;
/* USB data transfer */
/* PIPEn Buffer counter */
uint32_t		usb_gcstd_DataCnt[USBC_MAX_PIPE_NO + 1u];
/* DMA0 direction */
uint16_t		usb_gcstd_Dma0Dir;
/* DMA0 buffer size */
uint16_t		usb_gcstd_Dma0Size;
/* DMA0 FIFO buffer size */
uint16_t		usb_gcstd_Dma0Fifo;
/* DMA0 pipe number */
uint16_t		usb_gcstd_Dma0Pipe;
/* PIPEn Buffer pointer(8bit) */
uint8_t			*usb_gcstd_DataPtr[USBC_MAX_PIPE_NO + 1u];
/* Ignore count */
uint16_t		usb_ghstd_IgnoreCnt[USBC_MAX_PIPE_NO + 1u];
/* Message pipe */
USBC_UTR_t		*usb_gcstd_Pipe[USBC_MAX_PIPE_NO + 1u];
/* XCKE Mode Flag */
uint16_t		usb_gcstd_XckeMode;
/* Hi-speed enable */
uint16_t		usb_gcstd_HsEnable;


/******************************************************************************
Renesas Abstracted Driver API functions
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_BrdyEnable
Description     : Enable pipe BRDY interrupt
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_BrdyEnable(uint16_t pipe)
{
	/* Enable BRDY */
	USB_SET_PAT(BRDYENB, USBC_BITSET(pipe));
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_BrdyDisable
Description     : Disable pipe BRDY interrupt
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_BrdyDisable(uint16_t pipe)
{
	/* Disable BRDY */
	USB_CLR_PAT(BRDYENB, USBC_BITSET(pipe));
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_BempEnable
Description     : Enable pipe BEMP interrupt
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_BempEnable(uint16_t pipe)
{
	/* Enable BEMP */
	USB_SET_PAT(BEMPENB, USBC_BITSET(pipe));
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_BempDisable
Description     : Disable pipe BEMP interrupt
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_BempDisable(uint16_t pipe)
{
	/* Disable BEMP */
	USB_CLR_PAT(BEMPENB, USBC_BITSET(pipe));
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_NrdyEnable
Description     : Enable pipe NRDY interrupt
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_NrdyEnable(uint16_t pipe)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* 	At the peripheral operation, interrupt is disenabled */
	/* 	because handler becomes busy. */
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	/* Enable NRDY */
	USB_SET_PAT(NRDYENB, USBC_BITSET(pipe));
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	/* Check current function */
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{
		/* 	At the peripheral operation, interrupt is disenabled, */
		/*  because handler becomes busy. */
	}
	else
	{
		/* Enable NRDY */
		USB_SET_PAT(NRDYENB, USBC_BITSET(pipe));
	}
#endif	/* USBC_HOST_PERI_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_NrdyDisable
Description     : Disable pipe NRDY interrupt
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_NrdyDisable(uint16_t pipe)
{
	/* Disable NRDY */
	USB_CLR_PAT(NRDYENB, USBC_BITSET(pipe));
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_BerneEnable
Description     : Enable BEMP/BRDY/NRDY interrupt
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_BerneEnable(void)
{
	/* Enable BEMP, NRDY, BRDY */
	USB_SET_PAT(INTENB0, (USBC_BEMPE|USBC_NRDYE|USBC_BRDYE));
}
/******************************************************************************
End of function
******************************************************************************/



/******************************************************************************
Function Name   : usb_cstd_Epadr2Pipe
Description     : Endpoint address to pipe number
Arguments       : uint16_t dir_ep		; Direction + Endpoint Number
Return value    : uint16_t				; OK	: Pipe Number
                :                       ; ERROR : ERROR
******************************************************************************/
uint16_t usb_cstd_Epadr2Pipe(uint16_t dir_ep)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	uint16_t		i, conf, direp, tmp, *table;

	/* Check config number */
	conf = usb_gpstd_ConfigNum;
	if( conf < (uint16_t)1 )
	{
		/* Address state */
		conf = (uint16_t)1;
	}

	/* Peripheral */
	/* Get PIPE Number from Endpoint address */
	table = usb_gpstd_Driver.pipetbl[conf - 1];
	direp = (uint16_t)(((dir_ep & 0x80u) >> 3) | (dir_ep & 0x0Fu));
	/* EP table loop */
	for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
	{
		/* EP table endpoint dir check */
		tmp = (uint16_t)(table[i + 1] & (USBC_DIRFIELD | USBC_EPNUMFIELD));
		if( direp == tmp )
		{
			return table[i];
		}
	}
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	uint16_t		i, md, direp, tmp, *table;
	USBC_HCDREG_t	*driver;

	/* Host */
	/* Get PIPE Number from Endpoint address */
	for( md = 0; md < usb_ghstd_DeviceNum; md++ )
	{
		if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
			(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
		{
			driver = &usb_ghstd_DeviceDrv[md];
			table = driver->pipetbl;
			direp = (uint16_t)((((uint16_t)(dir_ep & 0x80u) ^ 0x80u) >> 3u)
								| (dir_ep & 0x0Fu));
			/* EP table loop */
			for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
			{
				tmp = (uint16_t)(table[i + 1] & (USBC_DIRFIELD
								| USBC_EPNUMFIELD));
				/* EP table endpoint dir check */
				if( direp == tmp )
				{
					return table[i];
				}
			}
		}
	}
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	uint16_t		i, md, conf, direp, tmp, *table;
	USBC_HCDREG_t	*driver;

	/* Check current function */
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{
		conf = usb_gpstd_ConfigNum;
		if( conf < (uint16_t)1 )
		{
			/* Address state */
			conf = (uint16_t)1;
		}

		/* Peripheral */
		/* Get PIPE Number from Endpoint address */
		table = (uint16_t*)((uint16_t**)
			(usb_gpstd_Driver.pipetbl[conf - 1]));
		direp = (uint16_t)(((dir_ep & 0x80) >> 3) | (dir_ep & 0x0F));
		/* EP table loop */
		for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
		{
			tmp = (uint16_t)
				(table[i + 1] & (USBC_DIRFIELD | USBC_EPNUMFIELD));
			/* EP table endpoint dir check */
			if( direp == tmp )
			{
				return table[i];
			}
		}
	}
	else
	{
		/* Host */
		/* Get PIPE Number from Endpoint address */
		for( md = 0; md < usb_ghstd_DeviceNum; md++ )
		{
			if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
				(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
			{
				driver = (USBC_HCDREG_t*)&usb_ghstd_DeviceDrv[md];
				table = (uint16_t*)(driver->pipetbl);
				direp = (uint16_t)((((dir_ep & 0x80) ^ 0x80) >> 3)
									| (dir_ep & 0x0F));
				/* EP table loop */
				for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
				{
					tmp = (uint16_t)(table[i + 1] & (USBC_DIRFIELD
									| USBC_EPNUMFIELD));
					/* EP table endpoint dir check */
					if( direp == tmp )
					{
						return table[i];
					}
				}
			}
		}
	}
#endif	/* USBC_HOST_PERI_PP */

	return USBC_ERROR;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Pipe2Epadr
Description     : pipe number to endpoint address
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint8_t				; OK	: Endpoint Number + Direction
                :                       ; ERROR : ERROR
******************************************************************************/
uint8_t usb_cstd_Pipe2Epadr(uint16_t pipe)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* Peripheral */
	USBC_PRINTF0("Not support peripheral function\n");
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	USBC_HCDREG_t	*driver;
	uint16_t		i, md, direp, *table;

	/* Host */
	/* Get Endpoint address from PIPE number */
	for( md = 0; md < usb_ghstd_DeviceNum; md++ )
	{
		if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
			(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
		{
			driver = &usb_ghstd_DeviceDrv[md];
			table = driver->pipetbl;
			/* EP table loop */
			for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
			{
				if( table[i] == pipe )
				{
					direp = (uint16_t)((((table[i + 1] & USBC_DIRFIELD)
						^ USBC_DIRFIELD) << 3)
							+ (table[i + 1] & USBC_EPNUMFIELD));
					return (uint8_t)(direp);
				}
			}
		}
	}
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	USBC_HCDREG_t	*driver;
	uint16_t		i, md, direp, *table;

	/* Check current function */
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{
		/* Peripheral */
		USBC_PRINTF0("Not support peripheral function\n");
		return (uint8_t)USBC_ERROR;
	}
	else
	{
		/* Host */
		/* Get Endpoint address from PIPE number */
		for( md = 0; md < usb_ghstd_DeviceNum; md++ )
		{
			if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
				(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
			{
				driver = (USBC_HCDREG_t*)&usb_ghstd_DeviceDrv[md];
				table = (uint16_t*)(driver->pipetbl);
				/* EP table loop */
				for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
				{
					if( table[i] == pipe )
					{
						direp = (uint16_t)((((table[i + 1] & USBC_DIRFIELD)
							^ USBC_DIRFIELD) << 3)
								+ (table[i + 1] & USBC_EPNUMFIELD));
						return (uint8_t)(direp);
					}
				}
			}
		}
	}
#endif	/* USBC_HOST_PERI_PP */

	return (uint8_t)USBC_ERROR;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_Pipe2Fport
Description     : pipe number to FIFO port select
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t				; FIFO port selector
******************************************************************************/
uint16_t usb_cstd_Pipe2Fport(uint16_t pipe)
{
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	uint16_t		i, conf, *table;

	conf = usb_gpstd_ConfigNum;
	if( conf < (uint16_t)1 )
	{
		/* Address state */
		conf = (uint16_t)1;
	}

	/* Peripheral */
	/* Get FIFO port from PIPE number */
	table = usb_gpstd_Driver.pipetbl[conf - 1];
	/* EP table loop */
	for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
	{
		if( table[i] == pipe )
		{
			return table[i + 5];
		}
	}
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	uint16_t		i, md, *table;
	USBC_HCDREG_t	*driver;

	/* Host */
	/* Get FIFO port from PIPE number */
	for( md = 0; md < usb_ghstd_DeviceNum; md++ )
	{
		if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
			(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
		{
			driver = &usb_ghstd_DeviceDrv[md];
			table = driver->pipetbl;
			/* EP table loop */
			for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
			{
				if( table[i] == pipe)
				{
					return table[i + 5];
				}
			}
		}
	}
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	uint16_t		i, md, conf, *table;
	USBC_HCDREG_t	*driver;

	/* Check current function */
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{

		conf = usb_gpstd_ConfigNum;
		if( conf < (uint16_t)1 )
		{
			/* Address state */
			conf = (uint16_t)1;
		}
		/* Peripheral */
		/* Get FIFO port from PIPE number */
		table = (uint16_t*)((uint16_t**)
			(usb_gpstd_Driver.pipetbl[conf - 1]));
		/* EP table loop */
		for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
		{
			if( table[i] == pipe )
			{
				return table[i + 5];
			}
		}
	}
	else
	{
		/* Host */
		/* Get FIFO port from PIPE number */
		for( md = 0; md < usb_ghstd_DeviceNum; md++ )
		{
			if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
				(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
			{
				driver = (USBC_HCDREG_t*)&usb_ghstd_DeviceDrv[md];
				table = (uint16_t*)(driver->pipetbl);
				/* EP table loop */
				for( i = 0; table[i] != USBC_PDTBLEND; i += USBC_EPL )
				{
					if( table[i] == pipe)
					{
						return table[i + 5];
					}
				}
			}
		}
	}
#endif	/* USBC_HOST_PERI_PP */

	return USBC_ERROR;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SwReset
Description     : Software reset
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_SwReset(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* USB Reset */
	USB_CLR_PAT(SYSCFG, USBC_USBE);
	/* USB Enable */
	USB_SET_PAT(SYSCFG, USBC_USBE);
#else	/* !USBC_RX600_PP */
	/* USB Reset */
	USB_CLR_PAT(SYSCFG0, USBC_USBE);
	/* USB Enable */
	USB_SET_PAT(SYSCFG0, USBC_USBE);
#endif	/* USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetDeviceAddress
Description     : Get device address from pipe number
Arguments       : uint16_t pipe				; Pipe Number
Return value    : uint16_t DEVSEL bit status
******************************************************************************/
uint16_t usb_cstd_GetDeviceAddress(uint16_t pipe)
{

/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	uint16_t		buffer;

	/* Peripheral */
	/* USB address */
	USB_RD(USBADDR, buffer);
	/* Device address */
	return (uint16_t)(buffer & USBC_USBADDR_MASK);
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	uint16_t		i, md, buffer;
	USBC_HCDREG_t	*driver;

	/* Host */
	if( pipe == USBC_PIPE0 )
	{
		USB_RD(DCPMAXP, buffer);
		/* Device address */
		return (uint16_t)(buffer & USBC_DEVSEL);
	}
	else
	{
		for( md = 0; md < usb_ghstd_DeviceNum; md++ )
		{
			if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
				(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
			{
				driver = &usb_ghstd_DeviceDrv[md];
				/* EP table loop */
				for( i = 0; driver->pipetbl[i] != USBC_PDTBLEND; i 
					+= USBC_EPL )
				{
					if( driver->pipetbl[i] == pipe )
					{
						buffer = driver->pipetbl[i + 3];
						/* Device address */
						return (uint16_t)(buffer & USBC_DEVSEL);
					}
				}
			}
		}
	}
	return USBC_ERROR;
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	uint16_t		i, md, buffer;
	USBC_HCDREG_t	*driver;

	/* Check current function */
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{
		/* Peripheral */
		/* USB address */
		USB_RD(USBADDR, buffer);
		/* Device address */
		return (uint16_t)(buffer & USBC_USBADDR_MASK);
	}
	else
	{
		/* Host */
		if( pipe == USBC_PIPE0 )
		{
			USB_RD(DCPMAXP, buffer);
			/* Device address */
			return (uint16_t)(buffer & USBC_DEVSEL);
		}
		else
		{
			for( md = 0; md < usb_ghstd_DeviceNum; md++ )
			{
				if( (usb_ghstd_DeviceDrv[md].ifclass != USBC_IFCLS_NOT) &&
					(usb_ghstd_DeviceDrv[md].devaddr != USBC_NODEVICE) )
				{
					driver = (USBC_HCDREG_t*)&usb_ghstd_DeviceDrv[md];
					/* EP table loop */
					for( i = 0; driver->pipetbl[i] != USBC_PDTBLEND; i
						+= USBC_EPL )
					{
						if( driver->pipetbl[i] == pipe )
						{
							buffer = driver->pipetbl[i + 3];
							/* Device address */
							return (uint16_t)(buffer & USBC_DEVSEL);
						}
					}
				}
			}
		}
	}
	return USBC_ERROR;
#endif	/* USBC_HOST_PERI_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoSqtgl
Description     : Do pipe SQTGL
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoSqtgl(uint16_t pipe, uint16_t toggle)
{
	/* Check toggle */
	if( (toggle & USBC_SQMON) == USBC_SQMON )
	{
		/* Do pipe SQSET */
		usb_cstd_DoSqset(pipe);
	}
	else
	{
		/* Do pipe SQCLR */
		usb_cstd_DoSqclr(pipe);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetBufSize
Description     : Return Buf size
Arguments       : uint16_t pipe				; Pipe Number
Return value    : uint16_t FIFO Buffer Size or Max packet size
******************************************************************************/
uint16_t usb_cstd_GetBufSize(uint16_t pipe)
{
	uint16_t	size, buffer;

	if( pipe == USBC_PIPE0 )
	{
		USB_RD(DCPCFG, buffer);
		if( (buffer & USBC_CNTMDFIELD) == USBC_CNTMDFIELD )
		{
			/* Continuation transmit */
			/* Buffer Size */
			size = USBC_PIPE0BUF;
		}
		else
		{
			/* Not continuation transmit */
			USB_RD(DCPMAXP, buffer);
			/* Max Packet Size */
			size = (uint16_t)(buffer & USBC_MAXP);
		}
	}
	else
	{
		/* Pipe select */
		USB_WR(PIPESEL, pipe);
		/* Read CNTMD */
		USB_RD(PIPECFG, buffer);
		if( (buffer & USBC_CNTMDFIELD) == USBC_CNTMDFIELD )
		{
			/* Continuation transmit */
/* Condition compilation by the difference of IP */
#if USBC_PIPEBUF_MODE_PP == USBC_PIPEBUF_FIX_PP
			USB_RD(PIPEMAXP, buffer);
			/* Max Packet Size */
			size = (uint16_t)(buffer & USBC_MXPS);
#else	/* !USBC_PIPEBUF_FIX_PP */
			USB_RD(PIPEBUF, buffer);
			/* Buffer Size */
			size = (uint16_t)((uint16_t)((buffer >> 10) + 1) * USBC_PIPEXBUF);
#endif	/* USBC_PIPEBUF_FIX_PP */
		}
		else
		{
			USB_RD(PIPEMAXP, buffer);
			/* Max Packet Size */
			size = (uint16_t)(buffer & USBC_MXPS);
		}
	}
	return size;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetMaxPacketSize
Description     : Get max packet size from pipe number
Arguments       : uint16_t pipe				; Pipe Number
Return value    : uint16_t Max Packet Size
******************************************************************************/
uint16_t usb_cstd_GetMaxPacketSize(uint16_t pipe)
{
	uint16_t	size, buffer;

	if( pipe == USBC_PIPE0 )
	{
		USB_RD(DCPMAXP, buffer);
		/* Max Packet Size */
		size = (uint16_t)(buffer & USBC_MAXP);
	}
	else
	{
		/* Pipe select */
		USB_WR(PIPESEL, pipe);
		USB_RD(PIPEMAXP, buffer);
		/* Max Packet Size */
		size = (uint16_t)(buffer & USBC_MXPS);
	}
	return size;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetDevsel
Description     : Get device address from pipe number
Arguments       : uint16_t pipe				; Pipe Number
Return value    : uint16_t DEVSEL bit status
******************************************************************************/
uint16_t usb_cstd_GetDevsel(uint16_t pipe)
{
	uint16_t	devsel, buffer;
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_PERI_PP
	/* USB address */
	USB_RD(USBADDR, buffer);
	/* Device address */
	devsel = (uint16_t)(buffer & USBC_USBADDR_MASK);
#endif	/* USBC_PERI_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	if( pipe == USBC_PIPE0 )
	{
		USB_RD(DCPMAXP, buffer);
	}
	else
	{
		/* Pipe select */
		USB_WR(PIPESEL, pipe);
		USB_RD(PIPEMAXP, buffer);
	}
	/* Device address */
	devsel = (uint16_t)(buffer & USBC_DEVSEL);
#endif	/* USBC_HOST_PP */
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PERI_PP
	/* Check current function */
	if( usb_cstd_FunctionUsbip() == USBC_NO )
	{
		/* Peripheral Function */
		/* USB address */
		USB_RD(USBADDR, buffer);
		/* Device address */
		devsel = (uint16_t)(buffer & USBC_USBADDR_MASK);
	}
	else
	{
		if( pipe == USBC_PIPE0 )
		{
			USB_RD(DCPMAXP, buffer);
		}
		else
		{
			/* Pipe select */
			USB_WR(PIPESEL, pipe);
			USB_RD(PIPEMAXP, buffer);
		}
		/* Device address */
		devsel = (uint16_t)(buffer & USBC_DEVSEL);
	}
#endif	/* USBC_HOST_PERI_PP */

	return devsel;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetPipeDir
Description     : Get PIPE DIR
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t Pipe direction
******************************************************************************/
uint16_t usb_cstd_GetPipeDir(uint16_t pipe)
{
	uint16_t		buffer;

	/* Pipe select */
	USB_WR(PIPESEL, pipe);
	/* Read Pipe direction */
	USB_RD(PIPECFG, buffer);
	return (uint16_t)(buffer & USBC_DIRFIELD);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetPipeType
Description     : Check PIPE TYPE
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t Pipe type
******************************************************************************/
uint16_t usb_cstd_GetPipeType(uint16_t pipe)
{
	uint16_t		buffer;

	/* Pipe select */
	USB_WR(PIPESEL, pipe);
	/* Read Pipe direction */
	USB_RD(PIPECFG, buffer);
	return (uint16_t)(buffer & USBC_TYPFIELD);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_PipeClr
Description     : Clear PIPE Setting
Arguments       : uint16_t pipe			; Pipe Number
 * 				: uint16_t *tbl			; ep table
 * 				: uint16_t ofs			; ep table offset
Return value    : none
******************************************************************************/
void usb_cstd_PipeClr(uint16_t pipe, uint16_t *tbl, uint16_t ofs)
{
	uint16_t		bit;

	usb_gcstd_Pipe[pipe] = (USBC_UTR_t*)USBC_NULL;
	bit = USBC_BITSET(pipe);

	/* Interrupt Disable */
	/* Ready		 Int Disable */
	USB_CLR_PAT(BRDYENB, bit);
	/* NotReady		 Int Disable */
	USB_CLR_PAT(NRDYENB, bit);
	/* Empty/SizeErr Int Disable */
	USB_CLR_PAT(BEMPENB, bit);
	/* PID=NAK & clear STALL */
	usb_cstd_ClrStall(pipe);
	/* PIPE Configuration */
	USB_WR(PIPESEL,	 pipe);
	USB_WR(PIPECFG,	 tbl[ofs + 1]);
/* Condition compilation by the difference of IP */
#if USBC_PIPEBUF_MODE_PP == USBC_PIPEBUF_FIX_PP
	/* Nothing */
#else	/* !USBC_PIPEBUF_FIX_PP */
	USB_WR(PIPEBUF,	 tbl[ofs + 2]);
#endif	/* USBC_PIPEBUF_FIX_PP */

	USB_WR(PIPEMAXP, tbl[ofs + 3]);
	USB_WR(PIPEPERI, tbl[ofs + 4]);
	/* FIFO buffer DATA-PID initialized */
	USB_WR(PIPESEL, USBC_PIPE0);
	/* SQCLR */
	usb_cstd_DoSqclr(pipe);
	/* ACLRM */
	usb_cstd_DoAclrm(pipe);
	/* CSSTS */
	usb_cstd_DoCsclr(pipe);
	/* Interrupt status clear */
	/* Ready		 Int Clear */
	USB_CLR_STS(BRDYSTS, bit);
	/* NotReady		 Int Clear */
	USB_CLR_STS(NRDYSTS, bit);
	/* Empty/SizeErr Int Clear */
	USB_CLR_STS(BEMPSTS, bit);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClrPipeCnfg
Description     : Clear pipe configuration register
Arguments       : uint16_t pipe_no			; pipe number
Return value    : none
******************************************************************************/
void usb_cstd_ClrPipeCnfg(uint16_t pipe_no)
{
	uint16_t	buffer, bit;

	usb_gcstd_Pipe[pipe_no] = (USBC_UTR_t*)USBC_NULL;
	bit = USBC_BITSET(pipe_no);

	/* PID=NAK & clear STALL */
	usb_cstd_ClrStall(pipe_no);
	/* Interrupt disable */
	/* Ready		 Int Disable */
	USB_CLR_PAT(BRDYENB, bit);
	/* NotReady		 Int Disable */
	USB_CLR_PAT(NRDYENB, bit);
	/* Empty/SizeErr Int Disable */
	USB_CLR_PAT(BEMPENB, bit);
	/* PIPE Configuration */
	usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_CUSE, USBC_NO);
	/* Clear D0FIFO-port */
	USB_RD(D0FIFOSEL, buffer);
	if( (buffer & USBC_CURPIPE) == pipe_no )
	{
		usb_cstd_StopDma();
		usb_cstd_D0fifoStopUsb();
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0
			, (uint16_t)USBC_D0USE, USBC_NO);
	}
	/* Clear D1FIFO-port */
	USB_RD(D1FIFOSEL, buffer);
	if( (buffer & USBC_CURPIPE) == pipe_no )
	{
		USB_MDF_PAT(D1FIFOSEL, USB_D1FIFO_MBW, USBC_MBW);
		usb_cstd_FPortChange2((uint16_t)USBC_PIPE0, (uint16_t)USBC_D1USE
			, USBC_NO);
	}
	USB_WR(PIPESEL,	 pipe_no);
	USB_WR(PIPECFG,	 0);
/* Condition compilation by the difference of IP */
#if USBC_PIPEBUF_MODE_PP == USBC_PIPEBUF_FIX_PP
	/* Nothing */
#else	/* !USBC_PIPEBUF_FIX_PP */
	USB_WR(PIPEBUF,	 0);
#endif	/* USBC_PIPEBUF_FIX_PP */

	USB_WR(PIPEMAXP, 0);
	USB_WR(PIPEPERI, 0);
	USB_WR(PIPESEL,	 USBC_PIPE0);
	/* FIFO buffer DATA-PID initialized */
	/* SQCLR */
	usb_cstd_DoSqclr(pipe_no);
	/* ACLRM */
	usb_cstd_DoAclrm(pipe_no);
	/* CSSTS */
	usb_cstd_DoCsclr(pipe_no);
	usb_cstd_ClrTransactionCounter(pipe_no);
	/* Interrupt status clear */
	/* Ready		 Int Clear */
	USB_CLR_STS(BRDYSTS, bit);
	/* NotReady		 Int Clear */
	USB_CLR_STS(NRDYSTS, bit);
	/* Empty/SizeErr Int Clear */
	USB_CLR_STS(BEMPSTS, bit);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_RegClrPipeCtr
Description     : PIPE control reg clear
Arguments       : uint16_t pipe         : pipe number
                : uint16_t bit          : clear pipe position
Return value    : none
******************************************************************************/
void usb_cstd_RegClrPipeCtr(uint16_t pipe, uint16_t bit)
{
	/* Select PIPE */
	switch( pipe )
	{
	/* PIPE control reg clear */
	case USBC_PIPE0: USB_CLR_PAT(DCPCTR,	  bit);	break;
	case USBC_PIPE1: USB_CLR_PAT(PIPE1CTR, bit);	break;
	case USBC_PIPE2: USB_CLR_PAT(PIPE2CTR, bit);	break;
	case USBC_PIPE3: USB_CLR_PAT(PIPE3CTR, bit);	break;
	case USBC_PIPE4: USB_CLR_PAT(PIPE4CTR, bit);	break;
	case USBC_PIPE5: USB_CLR_PAT(PIPE5CTR, bit);	break;
	case USBC_PIPE6: USB_CLR_PAT(PIPE6CTR, bit);	break;
	case USBC_PIPE7: USB_CLR_PAT(PIPE7CTR, bit);	break;
	case USBC_PIPE8: USB_CLR_PAT(PIPE8CTR, bit);	break;
	case USBC_PIPE9: USB_CLR_PAT(PIPE9CTR, bit);	break;
	default:									break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_RegSetPipeCtr
Description     : PIPE control reg set
Arguments       : uint16_t pipe         : pipe number
                : uint16_t bit          : clear pipe position
Return value    : none
******************************************************************************/
void usb_cstd_RegSetPipeCtr(uint16_t pipe, uint16_t bit)
{
	/* Select PIPE */
	switch( pipe )
	{
	/* PIPE control reg set */
	case USBC_PIPE0: USB_SET_PAT(DCPCTR,	  bit);	break;
	case USBC_PIPE1: USB_SET_PAT(PIPE1CTR, bit);	break;
	case USBC_PIPE2: USB_SET_PAT(PIPE2CTR, bit);	break;
	case USBC_PIPE3: USB_SET_PAT(PIPE3CTR, bit);	break;
	case USBC_PIPE4: USB_SET_PAT(PIPE4CTR, bit);	break;
	case USBC_PIPE5: USB_SET_PAT(PIPE5CTR, bit);	break;
	case USBC_PIPE6: USB_SET_PAT(PIPE6CTR, bit);	break;
	case USBC_PIPE7: USB_SET_PAT(PIPE7CTR, bit);	break;
	case USBC_PIPE8: USB_SET_PAT(PIPE8CTR, bit);	break;
	case USBC_PIPE9: USB_SET_PAT(PIPE9CTR, bit);	break;
	default:									break;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_RegRdCtr
Description     : PIPE control reg read
Arguments       : uint16_t pipe         : pipe number
Return value    : uint16_t              : PIPE Control Reg Read
******************************************************************************/
uint16_t usb_cstd_RegRdCtr(uint16_t pipe)
{
	uint16_t	buf;

	/* Select PIPE */
	switch( pipe )
	{
	/* PIPE control reg read */
	case USBC_PIPE0: USB_RD(DCPCTR,   buf);	break;
	case USBC_PIPE1: USB_RD(PIPE1CTR, buf);	break;
	case USBC_PIPE2: USB_RD(PIPE2CTR, buf);	break;
	case USBC_PIPE3: USB_RD(PIPE3CTR, buf);	break;
	case USBC_PIPE4: USB_RD(PIPE4CTR, buf);	break;
	case USBC_PIPE5: USB_RD(PIPE5CTR, buf);	break;
	case USBC_PIPE6: USB_RD(PIPE6CTR, buf);	break;
	case USBC_PIPE7: USB_RD(PIPE7CTR, buf);	break;
	case USBC_PIPE8: USB_RD(PIPE8CTR, buf);	break;
	case USBC_PIPE9: USB_RD(PIPE9CTR, buf);	break;
	default:		buf = 0;				break;
	}
	return buf;
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_FunctionUsbip
Description     : Check current function
Arguments       : none
Return value    : uint16_t			; YES : Host function
                :					; NO  : Peripheral function
******************************************************************************/
uint16_t usb_cstd_FunctionUsbip(void)
{
	uint16_t	buf;
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_RD(SYSCFG, buf);
#else	/* !USBC_RX600_PP */
	USB_RD(SYSCFG0, buf);
#endif	/* USBC_RX600_PP */
	if( (buf & USBC_DCFM) == USBC_DCFM )
	{
		/* Host Function mode */
		return USBC_YES;
	}
	else
	{
		/* Peripheral Function mode */
		return USBC_NO;
	}
}
/******************************************************************************
End of function
******************************************************************************/



/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if USBC_IPSEL_PP == USBC_597IP_PP

/******************************************************************************
Function Name   : usb_cstd_WaitUsbip
Description     : Wait USB ASSP ready
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_WaitUsbip(void)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
#else	/* !USBC_RX600_PP */
	uint16_t	buf;
#endif	/* USBC_RX600_PP */

	/* XCKE Mode Flag */
	usb_gcstd_XckeMode = USBC_NO;
	/* Hi-speed enable */
	usb_gcstd_HsEnable = USB_HSESEL;
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* Nothing */
#else	/* !USBC_RX600_PP */
	/*	Set base address */
	/*	USB register define	*/
	usb_gcstd_UsbReg = (struct USBC_REGISTER*)USB_BASE;

	do
	{
		USB_WR(SYSCFG0, USBC_USBE);
		USB_RD(SYSCFG0, buf);
	}
	while( (buf & USBC_USBE) == 0u );
	/* SW reset */
	USB_WR(SYSCFG0, 0x0);
	/* Crystal Select */
	USB_MDF_PAT(SYSCFG0, USBC_XINSEL, USBC_XTAL);
#endif	/* USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ReadBsts
Description     : Read pipe BSTS
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t BSTS bit status
******************************************************************************/
uint16_t usb_cstd_ReadBsts(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (uint16_t)(buf & USBC_BSTS);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ReadInbufm
Description     : Read pipe USBC_INBUFM
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t USBC_INBUFM bit status
******************************************************************************/
uint16_t usb_cstd_ReadInbufm(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (uint16_t)(buf & USBC_INBUFM);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoCsclr
Description     : Do pipe CSCLR
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoCsclr(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, (uint16_t)USBC_CSCLR);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoAclrm
Description     : Do pipe ACLRM
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoAclrm(uint16_t pipe)
{
	/* Control ACLRM */
	usb_cstd_SetAclrm(pipe);
	usb_cstd_ClrAclrm(pipe);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetAclrm
Description     : Set pipe ACLRM bit
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetAclrm(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, (uint16_t)USBC_ACLRM);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClrAclrm
Description     : Clear pipe ACLRM bit
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_ClrAclrm(uint16_t pipe)
{
	/* PIPE control reg clear */
	usb_cstd_RegClrPipeCtr(pipe, (uint16_t)USBC_ACLRM);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoSqclr
Description     : Do Spipe QCLR
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoSqclr(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, (uint16_t)USBC_SQCLR);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoSqset
Description     : Do pipe SQSET
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoSqset(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, (uint16_t)USBC_SQSET);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetBuf
Description     : Set pipe USBC_PID_BUF
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetBuf(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, (uint16_t)USBC_PID_BUF);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetNak
Description     : Set pipe PID_NAK
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetNak(uint16_t pipe)
{
	uint16_t		buf, n;

	/* Set NAK */
	usb_cstd_RegClrPipeCtr(pipe, (uint16_t)USBC_PID_BUF);
/* Condition compilation by the difference of USB function */
#if USB_FUNCSEL_PP == USBC_HOST_PP
	/* Wait CSSTS */
	do
	{
		/* PIPE control reg read */
		buf = usb_cstd_RegRdCtr(pipe);
	}
	while( (uint16_t)(buf & USBC_CSSTS) == USBC_CSSTS );
#endif	/* USBC_HOST_PP */

	for( n = 0; n < 0xFFFFu; ++n )
	{
		/* PIPE control reg read */
		buf = usb_cstd_RegRdCtr(pipe);
		if( (uint16_t)(buf & USBC_PBUSY) == 0 )
		{
			n = 0xFFFEu;
		}
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetStall
Description     : Set pipe USBC_PID_STALL
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetStall(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, (uint16_t)USBC_PID_STALL);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClrStall
Description     : Clear pipe USBC_PID_STALL
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
Note            : PID is set to NAK.
******************************************************************************/
void usb_cstd_ClrStall(uint16_t pipe)
{
	/* Set NAK */
	usb_cstd_SetNak(pipe);
	/* Set STALL */
	usb_cstd_RegClrPipeCtr(pipe, (uint16_t)USBC_PID_STALL);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetPid
Description     : Get Pipe PID from pipe number
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t PID bit status
******************************************************************************/
uint16_t usb_cstd_GetPid(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (uint16_t)(buf & USBC_PID);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ReadPipectr
Description     : Read pipe control register
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t PIPExCTR register
******************************************************************************/
uint16_t usb_cstd_ReadPipectr(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (buf);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_VbintClearSts
Description     : Clear VBINT interrupt status
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_VbintClearSts(void)
{
	/* Status Clear */
	USB_CLR_STS(INTSTS0, USBC_VBINT);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ResmClearSts
Description     : Clear RESM interrupt status
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_ResmClearSts(void)
{
	/* Status Clear */
	USB_CLR_STS(INTSTS0, USBC_RESM);
}
/******************************************************************************
End of function
******************************************************************************/


/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_1PORT_PP
/******************************************************************************
Function Name   : usb_cstd_PortSpeed
Description     : Check current port speed
Arguments       : uint16_t port		; Root port
Return value    : uint16_t			; HSCONNECT : Hi-Speed
                :					; FSCONNECT : Full-Speed
                :					; LSCONNECT : Low-Speed
                :					; NOCONNECT : not connect
******************************************************************************/
uint16_t usb_cstd_PortSpeed(uint16_t port)
{
	uint16_t	buf, ConnInf;

	/* Reset handshake status get */
	USB_RD(DVSTCTR0, buf);
	buf = (uint16_t)(buf & USBC_RHST);

	/* Check RHST */
	switch( buf )
	{
	/* Get Reset handshake status */
	case USBC_HSMODE:	ConnInf = USBC_HSCONNECT;	break;
	case USBC_FSMODE:	ConnInf = USBC_FSCONNECT;	break;
	case USBC_LSMODE:	ConnInf = USBC_LSCONNECT;	break;
	case USBC_HSPROC:	ConnInf = USBC_NOCONNECT;	break;
	default:		ConnInf = USBC_NOCONNECT;	break;
	}

	return (ConnInf);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_HiSpeedEnable
Description     : Check Hi-Speed enable bit
Arguments       : uint16_t port		; Root port
Return value    : uint16_t			; YES : Hi-Speed Enable
                :					; NO  : Hi-Speed Disable
******************************************************************************/
uint16_t usb_cstd_HiSpeedEnable(uint16_t port)
{
	uint16_t	buf;
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_RD(SYSCFG, buf);
#else	/* !USBC_RX600_PP */
	USB_RD(SYSCFG0, buf);
#endif	/* USBC_RX600_PP */

	/* Hi-speed enable check */
	if( (buf & USBC_HSE) == USBC_HSE )
	{
		/* Hi-Speed Enable */
		return USBC_YES;
	}
	else
	{
		/* Hi-Speed Disable */
		return USBC_NO;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetHse
Description     : Enable port HSE
Arguments       : uint16_t port			; Root port
                : uint16_t speed			; HS_ENABLE/HS_DISABLE
Return value    : none
******************************************************************************/
void usb_cstd_SetHse(uint16_t port, uint16_t speed)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	if( speed == USBC_HS_DISABLE )
	{
		/* HSE = disable */
		USB_CLR_PAT(SYSCFG, USBC_HSE);
	}
	else
	{
		/* HSE = enable */
		USB_SET_PAT(SYSCFG, USBC_HSE);
	}
#else	/* !USBC_RX600_PP */
	if( speed == USBC_HS_DISABLE )
	{
		/* HSE = disable */
		USB_CLR_PAT(SYSCFG0, USBC_HSE);
	}
	else
	{
		/* HSE = enable */
		USB_SET_PAT(SYSCFG0, USBC_HSE);
	}
#endif	/* USBC_RX600_PP */
}
#endif	/*  USBC_PORTSEL_PP == USBC_1PORT_PP */
/******************************************************************************
End of function
******************************************************************************/


/* Condition compilation by the difference of the devices */
#if USBC_PORTSEL_PP == USBC_2PORT_PP
/******************************************************************************
Function Name   : usb_cstd_PortSpeed
Description     : Check current port speed
Arguments       : uint16_t port		; Root port
Return value    : uint16_t			; HSCONNECT : Hi-Speed
                :					; FSCONNECT : Full-Speed
                :					; LSCONNECT : Low-Speed
                :					; NOCONNECT : not connect
******************************************************************************/
uint16_t usb_cstd_PortSpeed(uint16_t port)
{
	uint16_t	buf, ConnInf;

	/* Get Port information */
	if( port == USBC_PORT0 )
	{
		USB_RD(DVSTCTR0, buf);
	}
	else if( port == USBC_PORT1 )
	{
		USB_RD(DVSTCTR1, buf);
	}
	else
	{
		buf = USBC_UNDECID;
	}

	/* Reset handshake status get */
	buf = (uint16_t)(buf & USBC_RHST);

	switch( buf )
	{
	/* Get port speed */
	case USBC_HSMODE:	ConnInf = USBC_HSCONNECT;	break;
	case USBC_FSMODE:	ConnInf = USBC_FSCONNECT;	break;
	case USBC_LSMODE:	ConnInf = USBC_LSCONNECT;	break;
	case USBC_HSPROC:	ConnInf = USBC_NOCONNECT;	break;
	default:		ConnInf = USBC_NOCONNECT;	break;
	}

	return (ConnInf);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_HiSpeedEnable
Description     : Check Hi-Speed enable bit
Arguments       : uint16_t port		; Root port
Return value    : uint16_t			; YES : Hi-Speed Enable
                :					; NO  : Hi-Speed Disable
******************************************************************************/
uint16_t usb_cstd_HiSpeedEnable(uint16_t port)
{
	uint16_t	buf;

	if( port == USBC_PORT0 )
	{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		USB_RD(SYSCFG, buf);
#else	/* !USBC_RX600_PP */
		USB_RD(SYSCFG0, buf);
#endif	/* USBC_RX600_PP */
	}
	else if( port == USBC_PORT1 )
	{
		USB_RD(SYSCFG1, buf);
	}
	else
	{
		/* Hi-Speed Disable */
		return USBC_NO;
	}

	if( (buf & USBC_HSE) == USBC_HSE )
	{
		/* Hi-Speed Enable */
		return USBC_YES;
	}
	else
	{
		/* Hi-Speed Disable */
		return USBC_NO;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetHse
Description     : Enable port HSE
Arguments       : uint16_t port			; Root port
                : uint16_t speed			; HS_ENABLE/HS_DISABLE
Return value    : none
******************************************************************************/
void usb_cstd_SetHse(uint16_t port, uint16_t speed)
{
	if( port == USBC_PORT0 )
	{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		if( speed == USBC_HS_DISABLE )
		{
			/* HSE = disable */
			USB_CLR_PAT(SYSCFG, USBC_HSE);
		}
		else
		{
			/* HSE = enable */
			USB_SET_PAT(SYSCFG, USBC_HSE);
		}
#else	/* !USBC_RX600_PP */
		if( speed == USBC_HS_DISABLE )
		{
			/* HSE = disable */
			USB_CLR_PAT(SYSCFG0, USBC_HSE);
		}
		else
		{
			/* HSE = enable */
			USB_SET_PAT(SYSCFG0, USBC_HSE);
		}
#endif	/* USBC_RX600_PP */
	}
	else
	{
		if( speed == USBC_HS_DISABLE )
		{
			/* HSE = disable */
			USB_CLR_PAT(SYSCFG1, USBC_HSE);
		}
		else
		{
			/* HSE = enable */
			USB_SET_PAT(SYSCFG1, USBC_HSE);
		}
	}
}
#endif	/*  USBC_PORTSEL_PP == USBC_2PORT_PP */


#endif /* USBC_IPSEL_PP == USBC_597IP_PP */
/******************************************************************************
End of function
******************************************************************************/




/*===========================================================================*/
/* Condition compilation by the difference of IP */
#if (USBC_IPSEL_PP == USBC_596IP_PP) || (USBC_IPSEL_PP == USBC_592IP_PP)

/* 0(NO):OFF, 4:Peri, 5:Host PCUT mode flag */
uint16_t		usb_gcstd_PcutMode;


/******************************************************************************
Function Name   : usb_cstd_WaitUsbip
Description     : Wait USB ASSP ready
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_WaitUsbip(void)
{
	uint16_t	buf;

	/* XCKE Mode Flag */
	usb_gcstd_XckeMode = USBC_NO;
	/* PCUT Mode Flag */
	usb_gcstd_PcutMode = USBC_NO;
	/* Hi-speed enable */
	usb_gcstd_HsEnable = USB_HSESEL;

	/*	Set base address(usb register define) */
	usb_gcstd_UsbReg = (struct USBC_REGISTER*)USB_BASE;
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	/* Nothing */
#else	/* !USBC_RX600_PP */
	do
	{
		USB_WR(SYSCFG0, USBC_USBE);
		USB_RD(SYSCFG0, buf);
	}
	while( (buf & USBC_USBE) == 0u );
	/* SW reset */
	USB_WR(SYSCFG0, 0x0);
	/* Crystal Select */
	USB_MDF_PAT(SYSCFG0, USBC_XINSEL, USBC_XTAL);
#endif	/* USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_PortSpeed
Description     : Check current port speed
Arguments       : uint16_t port		; Root port
Return value    : uint16_t			; HSCONNECT : Hi-Speed
                :					; FSCONNECT : Full-Speed
                :					; LSCONNECT : Low-Speed
                :					; NOCONNECT : not connect
******************************************************************************/
uint16_t usb_cstd_PortSpeed(uint16_t port)
{
	uint16_t	buf, ConnInf;

	if( port == USBC_PORT0 )
	{
		USB_RD(DVSTCTR0, buf);
	}
	else
	{
		buf = USBC_UNDECID;
	}

	/* Reset handshake status get */
	buf = (uint16_t)(buf & USBC_RHST);

	switch( buf )
	{
	/* Get port speed */
	case USBC_HSMODE:	ConnInf = USBC_HSCONNECT;	break;
	case USBC_FSMODE:	ConnInf = USBC_FSCONNECT;	break;
	case USBC_HSPROC:	ConnInf = USBC_NOCONNECT;	break;
	default:		ConnInf = USBC_NOCONNECT;	break;
	}

	return (ConnInf);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_HiSpeedEnable
Description     : Check Hi-Speed enable bit
Arguments       : uint16_t port		; Root port
Return value    : uint16_t			; YES : Hi-Speed Enable
                :					; NO  : Hi-Speed Disable
******************************************************************************/
uint16_t usb_cstd_HiSpeedEnable(uint16_t port)
{
	uint16_t	buf;

	if( port == USBC_PORT0 )
	{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
		USB_RD(SYSCFG, buf);
#else	/* !USBC_RX600_PP */
		USB_RD(SYSCFG0, buf);
#endif	/* USBC_RX600_PP */
	}
	else
	{
		/* Hi-Speed Disable */
		return USBC_NO;
	}

	if( (buf & USBC_HSE) == USBC_HSE )
	{
		/* Hi-Speed Enable */
		return USBC_YES;
	}
	else
	{
		/* Hi-Speed Disable */
		return USBC_NO;
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ReadBsts
Description     : Read pipe BSTS
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t BSTS bit status
******************************************************************************/
uint16_t usb_cstd_ReadBsts(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (uint16_t)(buf & USBC_BSTS);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ReadInbufm
Description     : Read pipe USBC_INBUFM
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t USBC_INBUFM bit status
******************************************************************************/
uint16_t usb_cstd_ReadInbufm(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (uint16_t)(buf & USBC_INBUFM);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoCsclr
Description     : Do pipe CSCLR
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoCsclr(uint16_t pipe)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoAclrm
Description     : Do pipe ACLRM
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoAclrm(uint16_t pipe)
{
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetAclrm
Description     : Set pipe ACLRM bit
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetAclrm(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, USBC_ACLRM);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClrAclrm
Description     : Clear pipe ACLRM bit
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_ClrAclrm(uint16_t pipe)
{
	/* Clear PIPE ACLRM */
	usb_cstd_RegClrPipeCtr(pipe, USBC_ACLRM);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoSqclr
Description     : Do Spipe QCLR
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoSqclr(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, USBC_SQCLR);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_DoSqset
Description     : Do pipe SQSET
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_DoSqset(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, USBC_SQSET);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetBuf
Description     : Set pipe USBC_PID_BUF
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetBuf(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, USBC_PID_BUF);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetNak
Description     : Set pipe PID_NAK
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetNak(uint16_t pipe)
{
	/* Set PID=BUF */
	usb_cstd_RegClrPipeCtr(pipe, USBC_PID_BUF);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetStall
Description     : Set pipe USBC_PID_STALL
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
******************************************************************************/
void usb_cstd_SetStall(uint16_t pipe)
{
	/* PIPE control reg set */
	usb_cstd_RegSetPipeCtr(pipe, USBC_PID_STALL);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ClrStall
Description     : Clear pipe USBC_PID_STALL
Arguments       : uint16_t pipe			; Pipe Number
Return value    : none
Note            : PID is set to NAK.
******************************************************************************/
void usb_cstd_ClrStall(uint16_t pipe)
{
	/* Set NAK */
	usb_cstd_SetNak(pipe);
	/* Set STALL */
	usb_cstd_RegClrPipeCtr(pipe, USBC_PID_STALL);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_GetPid
Description     : Get Pipe PID from pipe number
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t PID bit status
******************************************************************************/
uint16_t usb_cstd_GetPid(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (uint16_t)(buf & USBC_PID);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ReadPipectr
Description     : Read pipe control register
Arguments       : uint16_t pipe			; Pipe Number
Return value    : uint16_t PIPExCTR register
******************************************************************************/
uint16_t usb_cstd_ReadPipectr(uint16_t pipe)
{
	uint16_t	buf;

	/* PIPE control reg read */
	buf = usb_cstd_RegRdCtr(pipe);
	return (buf);
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_SetHse
Description     : Enable port HSE
Arguments       : uint16_t port			; Root port
                : uint16_t speed			; HS_ENABLE/HS_DISABLE
Return value    : none
******************************************************************************/
void usb_cstd_SetHse(uint16_t port, uint16_t speed)
{
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	if( speed == USBC_HS_DISABLE )
	{
		/* HSE = disable */
		USB_CLR_PAT(SYSCFG, USBC_HSE);
	}
	else
	{
		/* HSE = enable */
		USB_SET_PAT(SYSCFG, USBC_HSE);
	}
#else	/* !USBC_RX600_PP */
	if( speed == USBC_HS_DISABLE )
	{
		/* HSE = disable */
		USB_CLR_PAT(SYSCFG0, USBC_HSE);
	}
	else
	{
		/* HSE = enable */
		USB_SET_PAT(SYSCFG0, USBC_HSE);
	}
#endif	/* USBC_RX600_PP */
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_VbintClearSts
Description     : Clear VBINT interrupt status
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_VbintClearSts(void)
{
	uint16_t	buf;

	/* Status Clear */
	USB_CLR_STS(INTSTS0, USBC_VBINT);
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_RD(SYSCFG, buf);
#else	/* !USBC_RX600_PP */
	USB_RD(SYSCFG0, buf);
#endif	/* USBC_RX600_PP */
	/* USB clock enable check */
	if( (uint16_t)(buf & USBC_SCKE) == 0 )
	{
		/* Status Clear (Clock stop) */
		USB_SET_STS(INTSTS0, USBC_VBINT);
	}
}
/******************************************************************************
End of function
******************************************************************************/


/******************************************************************************
Function Name   : usb_cstd_ResmClearSts
Description     : Clear RESM interrupt status
Arguments       : none
Return value    : none
******************************************************************************/
void usb_cstd_ResmClearSts(void)
{
	uint16_t	buf;

	/* Status Clear */
	USB_CLR_STS(INTSTS0, USBC_RESM);
/* Condition compilation by the difference of the devices */
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_RD(SYSCFG, buf);
#else	/* !USBC_RX600_PP */
	USB_RD(SYSCFG0, buf);
#endif	/* USBC_RX600_PP */
	/* USB clock enable check */
	if( (uint16_t)(buf & USBC_SCKE) == 0 )
	{
		/* Status Clear (Clock stop) */
		USB_SET_STS(INTSTS0, USBC_RESM);
	}
}


#endif /* USBC_IPSEL_PP == USBC_596IP_PP */
/******************************************************************************
End of function
******************************************************************************/

void usb_cstd_SetDma( void )
{
	/* Disable DREQ */
	USB_CLR_PAT(D0FIFOSEL, USBC_DREQE);
	/* DMA stop */
	usb_cstd_StopDma();

	/* Check direction */
	if( usb_gcstd_Dma0Dir == USBC_BUF2FIFO )
	{
		USB_CLR_STS(BEMPSTS, USBC_BITSET(usb_gcstd_Dma0Pipe));
		/* Enable Empty Interrupt */
		usb_cstd_BempEnable(usb_gcstd_Dma0Pipe);
		if( (usb_gcstd_Dma0Size % usb_gcstd_Dma0Fifo) != 0 )
		{
			/* DataSize % MXPS */
			/* Set BVAL */
			USB_WR(D0FIFOCTR, USBC_BVAL);
		}
	}
	else
	{
		usb_gcstd_DataCnt[usb_gcstd_Dma0Pipe] = 0;
	}
}
/******************************************************************************
End of function
******************************************************************************/
/******************************************************************************
End  Of File
******************************************************************************/




#if 1	// DEMO_PRG
void usb_cstd_UsbClear(void)
{
	/* CFIFO Port Select Register  (0x1E) */
	USB_WR(CFIFOSEL,  0);
	/* D0FIFO Port Select Register (0x24) */
	USB_WR(D0FIFOSEL, 0);
	/* D1FIFO Port Select Register (0x2A) */
	USB_WR(D1FIFOSEL, 0);
	
#if USBC_TARGET_CHIP_PP == USBC_RX600_PP
	USB_WR(SYSCFG,  0);
#else	/* !USBC_RX600_PP */
	USB_WR(SYSCFG0, 0);
#endif	/* USBC_RX600_PP */
}
#endif

